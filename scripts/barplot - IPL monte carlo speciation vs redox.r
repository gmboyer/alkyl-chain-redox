# plot boxplot in ggplot and plotly
library(plyr)
library(reshape2)
library(ggplot2)

# function to clean and prepare a data frame for plotly/ggplot plotting
clean <- function(y_to_plot){
  # remove entries with NA or NaN for y
  y_to_plot <- y_to_plot[complete.cases(y_to_plot),]
  # convert to a data frame, which can undergo binning
  y_to_plot <- as.data.frame(y_to_plot)
  # bin data for boxplots
  y_to_plot$bin <- cut(y_to_plot$x, c(-Inf, seq(min(y_to_plot$x), max(y_to_plot$x), length.out=75), Inf))
  # remove entries with NA or NaN for bins
  y_to_plot <- y_to_plot[complete.cases(y_to_plot),]
  return(y_to_plot)
}

create_mc_barplot <- function(spring, iter="undefined", min, max, by, metric="pe"){

  if(metric=="Eh"){
    xlab <- "Eh, volts"
  }else if(metric=="pe"){
    xlab <- "pe"
  }else{
    stop("Error in create_lineplot(): metric must be either 'Eh' or 'pe'.")
  }

  t <- readRDS(paste0("rds//monte//IPL_thermo_peak_area_RF_monte_", spring, ".rds"))

  number_of_ys <- ncol(t)-1

  if(grepl("BP", spring)){
    sample_prefix <- "BP"
  } else if(grepl("MS", spring)){
    sample_prefix <- "MS"
  } else if(grepl("EP", spring)){
    sample_prefix <- "EP"
  } else if(grepl("OS", spring)){
    sample_prefix <- "OS"
  }

  for(i in 1:number_of_ys){
    var_name <- paste("y", i, sep="")
    assign(var_name, t[var_name])
  }
  x  <- t$x[seq(1, nrow(y1))] # pe

  pal <- c("#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b")
  pal_2 <- c("black", "black", "black", "black", "black", "black")

  # palette list to evaluate in ggplot scale_fill_manual(values = xxx)
  pal_list_to_eval <- "c("
  pal_list_to_eval_2 <- "c("
  for(i in 1:number_of_ys-1){
    pal_list_to_eval <- paste(pal_list_to_eval, sample_prefix, i, " = pal[", i, "], ", sep="")
    pal_list_to_eval_2 <- paste(pal_list_to_eval_2, sample_prefix, i, " = pal_2[", i, "], ", sep="")
  }
  pal_list_to_eval <- paste(pal_list_to_eval, sample_prefix, number_of_ys, " = pal[", number_of_ys, "])", sep="")
  pal_list_to_eval_2 <- paste(pal_list_to_eval_2, sample_prefix, number_of_ys, " = pal_2[", number_of_ys, "])", sep="")

  # bind x and y of interest
  for(i in 1:number_of_ys){
    var_name <- paste("y", i, "_to_plot", sep="")
    assign(var_name, cbind(x = t$x, y = as.vector(unlist(t[paste("y", i, sep="")]))))
    assign(var_name, clean(get(var_name))) # cleans out incomplete x-y cases
    assign(var_name, cbind(get(var_name), site = paste(sample_prefix, i, sep="")))
  }

  y_to_plot <- y1_to_plot
  for(i in 2:number_of_ys){
    var_name <- paste("y", i, "_to_plot", sep="")
    y_to_plot <- rbind(y_to_plot, get(var_name))
  }
  y_to_plot <- as.data.frame(y_to_plot)

  p <- ggplot(y_to_plot, aes(x = factor(round_any(x, by)), y = y, fill = site, color = site)) +
      geom_boxplot(outlier.alpha = 0) +
      labs(x=xlab, y="% abundance") +
      # scale_x_discrete(breaks = round(seq(min(y_to_plot$x), max(y_to_plot$x), by = 0.05), 2))
      scale_x_discrete(labels = sprintf("%.2f", seq(min, max, by))) +
      scale_fill_manual(name = "Site", values = eval(parse(text = pal_list_to_eval))) +
      scale_color_manual(name = "Site", values = eval(parse(text = pal_list_to_eval_2))) +
      theme(
        axis.text.y = element_text(colour="black"),
        axis.text.x = element_text(colour="black", angle = 0),
        panel.background = element_blank(),
        legend.title = element_blank(), # remove legend title
        legend.key=element_blank() # remove gray background behind legend keys
      )

  dir.create(file.path(getwd(), "plots"), showWarnings = FALSE)

  pdf(file=paste0("plots//boxplot_ggplot_", as.character(by), "_bin_", spring, "_iter_", iter, "_redox.pdf"), width = 10, height = 4) # create pdf
  print(p)
  dev.off()
}
