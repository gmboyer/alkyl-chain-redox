library(plyr)
library(reshape2)
library(ggplot2)

# function to clean and prepare a data frame for plotly/ggplot plotting
cleanup_data <- function(y_to_plot){
  # remove entries with NA or NaN for y
  y_to_plot <- y_to_plot[complete.cases(y_to_plot),]
  # convert to a data frame, which can undergo binning
  y_to_plot <- as.data.frame(y_to_plot)
  # bin data for boxplots
  y_to_plot$bin <- cut(y_to_plot$x, c(-Inf, seq(min(y_to_plot$x), max(y_to_plot$x), length.out=75), Inf))
  # remove entries with NA or NaN for bins
  y_to_plot <- y_to_plot[complete.cases(y_to_plot),]
  return(y_to_plot)
}

create_lineplot_pe <- function(spring, min, max, by, metric="pe"){

  if(metric=="Eh"){
    xlab <- "Eh, volts"
  }else if(metric=="pe"){
    xlab <- "pe"
  }else{
    stop("Error in create_lineplot(): metric must be either 'Eh' or 'pe'.")
  }

  t <- readRDS(paste0("rds/thermo_results_", spring, ".rds"))

  number_of_ys <- ncol(t)-1

  if(grepl("BP", spring)){
    sample_prefix <- "BP"
  } else if(grepl("MS", spring)){
    sample_prefix <- "MS"
  } else if(grepl("EP", spring)){
    sample_prefix <- "EP"
  } else if(grepl("OS", spring)){
    sample_prefix <- "OS"
  }

  for(i in 1:number_of_ys){
    var_name <- paste("y", i, sep="")
    assign(var_name, t[var_name])
  }
  x  <- t$x[seq(1, nrow(y1))] # Eh

  pal <- c("#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b")
  pal_2 <- c("black", "black", "black", "black", "black", "black")

  # palette list to evaluate in ggplot scale_fill_manual(values = xxx)
  pal_list_to_eval <- "c("
  pal_list_to_eval_2 <- "c("
  for(i in 1:number_of_ys-1){
    pal_list_to_eval <- paste(pal_list_to_eval, sample_prefix, i, " = pal[", i, "], ", sep="")
    pal_list_to_eval_2 <- paste(pal_list_to_eval_2, sample_prefix, i, " = pal_2[", i, "], ", sep="")
  }
  pal_list_to_eval <- paste(pal_list_to_eval, sample_prefix, number_of_ys, " = pal[", number_of_ys, "])", sep="")
  pal_list_to_eval_2 <- paste(pal_list_to_eval_2, sample_prefix, number_of_ys, " = pal_2[", number_of_ys, "])", sep="")

  # bind x and y of interest
  for(i in 1:number_of_ys){
    var_name <- paste("y", i, "_to_plot", sep="")
    assign(var_name, cbind(x = t$x, y = as.vector(unlist(t[paste("y", i, sep="")]))))
    assign(var_name, cleanup_data(get(var_name))) # cleans out incomplete x-y cases
    assign(var_name, cbind(get(var_name), site = paste(sample_prefix, i, sep="")))
  }

  y_to_plot <- y1_to_plot
  for(i in 2:number_of_ys){
    var_name <- paste("y", i, "_to_plot", sep="")
    y_to_plot <- rbind(y_to_plot, get(var_name))
  }
  y_to_plot <- as.data.frame(y_to_plot)

  # plot boxplot in ggplot and plotly
  p <- ggplot(t, aes(x = x, y = y1)) +
         theme(panel.background = element_blank()) +
         # labs(x = "Eh, volts", y = "% abundance")
         labs(x = xlab, y = "degree of formation (%)")

  for(i in 1:number_of_ys){
    var_name = paste("y", i, sep="")
    site_name <- paste(sample_prefix, i, sep="")
    p <- p + geom_line(data=t, aes_string(x="x", y=var_name, colour = shQuote(site_name)), size=1, inherit.aes=FALSE)
  }

  # add vertical line at lipid-predicted Eh (max abundance) for the spring
  this_num <- substr(spring, nchar(spring), nchar(spring)) # the Sample number
  max_num <- substr(colnames(t)[ncol(t)], nchar(colnames(t)[ncol(t)]), nchar(colnames(t)[ncol(t)])) # last Sample number (e.g. "6" for Bison Pool)
  this_var <- paste0("y", this_num) # the variable of this spring, e.g. "y2" when calculated for conditions of Bison Sample2
  if(this_num == "1" | this_num == max_num){
    maxabund <- 95 # 95% height cutoff for first and last outflow sites
  } else {
    maxabund <- max(t[, this_var]) # maximum abundance of this site (e.g 89)
  }
  redox_predict <- t[which.min(abs(t[, this_var] - maxabund)), "x"] # find Eh closest to max abundance or 95% height cutoff
  # p <- p + geom_segment(aes(x = redox_predict, y = 0, xend = redox_predict, yend = maxabund), linetype = 2, colour = pal[as.numeric(this_num)]) # vline ends at max abund
  p <- p + geom_vline(xintercept = redox_predict, linetype = 2, size = 1, colour = pal[as.numeric(this_num)]) # vline spans entire plot


  p <- p + scale_colour_manual(name = sample_prefix, values = eval(parse(text = pal_list_to_eval)))
  p <- p + scale_x_continuous(breaks = seq(min, max, by))
  p <- p + theme(
    axis.text.y = element_text(colour="black"),
    axis.text.x = element_text(colour="black", angle = 0),
    panel.background = element_blank(),
    # legend.position = "none", # remove legend completely
    legend.title = element_blank(), # remove legend title
    legend.key=element_blank() # remove gray background behind legend keys

  )

  dir.create(file.path(getwd(), "plots"), showWarnings = FALSE)

  # png(file=paste0("plots//", spring, "_thermo_redox.png"), width=10, height=4, units = 'in', res = 600) # create png
  pdf(file=paste0("plots//", spring, "_thermo_redox.pdf"), width = 10, height = 4) # create pdf
  print(p)# + theme(legend.position="none"))
  dev.off()

  return(redox_predict)

}
