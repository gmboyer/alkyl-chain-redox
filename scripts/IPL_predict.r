library(CHNOSZ)
library(dplyr)

# get pe values associated with water stability regions at a given T, P, pH, and logaH2O (usually 0)
# adapted from CHNOSZ's water.lines(): https://rdrr.io/cran/CHNOSZ/src/R/util.plot.R
water_stability <- function(T=25, P=1, pH=7, logaH2O=0, units="Eh", which="reducing"){
  T <- T + 273.15
  if(which=="reducing"){
    logfH2 <- logaH2O
    logK <- suppressMessages(subcrt(c("H2O", "O2", "H2"), c(-1, 0.5, 1), c("liq", "aq", "gas"), T=T, P=P, convert=FALSE))$out$logK
    logfO2 <- 2 * (logK - logfH2 + logaH2O)
  }else if(which=="oxidizing"){
    logfO2 <- logaH2O
    logK <- suppressMessages(subcrt(c("O2", "O2"), c(-1, 1), c("gas", "aq"), T=T, P=P, convert=FALSE))$out$logK 
    logfO2 <- logfO2 + logK
  }else{
    stop("Error in water_stability: the 'which' arg must be either 'reducing' or 'oxidizing'.")
  }
    if(units=="Eh"){
      return(convert(logfO2, 'E0', T=T, P=P, pH=pH, logaH2O=logaH2O))
    } else if (units == 'pe'){
      return(convert(convert(logfO2, 'E0', T=T, P=P, pH=pH, logaH2O=logaH2O), "pe", T=T))
    } else {
      stop("Error in water_stability: units must be 'Eh' or 'pe'.")
    }
    

}

IPLthermo_directory <- "data//"
IPLworkbook_directory <- "data//"
res <- 800

# load current IPL_master
IPL_master <- readRDS("rds//IPL_master.rds")

# load Batch_EQ3 output
geo <- readRDS(paste0("rds//geo_", pred, ".rds"))

# create a dataframe of geochemical data
df <- geo$input_processed %>%
        na_if(0) %>%
        mutate(logCO2=log10(`HCO3-_Molality`)) %>%
        mutate(logNH3=log10(`NH4+_Molality`)) %>%
        mutate(logNO3=log10(`NO3-_Molality`)) %>%
        select(T="Temperature_degC", pH="H+_pH",
               logCO2, logNH3, logNO3)

rownames(df) <- geo$input_processed$Sample


# plotting ranges and parameters
calcrng <- list(Eh = list(BP=c(-0.56, -0.30, 0.02),
                         MS=c(-0.70, -0.40, 0.02),
                         EP=c(-0.50, -0.26, 0.02),
                         OS=c(-0.56, -0.30, 0.02)),
               pe = list(BP=c(-8, -5, 0.2),
                         MS=c(-10, -5.2, 0.4),
                         EP=c(-8, -4, 0.2),
                         OS=c(-8, -6, 0.1)))

pltrng <- list(Eh = list(BP=c(-0.56, -0.30, 0.02),
                         MS=c(-0.70, -0.40, 0.02),
                         EP=c(-0.50, -0.26, 0.02),
                         OS=c(-0.56, -0.30, 0.02)),
               pe = list(BP=c(0, 6, 1),
                         MS=c(0, 6, 1),
                         EP=c(0, 6, 1),
                         OS=c(0, 6, 1)))

calcrng[["my_species"]] <- list(BP=c(9, 10, 11, 12, 13, 14),
                               MS=c(15, 8, 16, 17, 18),
                               EP=c(3, 4, 5, 6, 7),
                               OS=c(2, 1))


if(pred=="Eh"){
  thermo_method <- "mosaic()"
  tablename <- "table - alkyl chain predicted Eh.csv"
}else if(pred == "pe"){
  thermo_method <- "mosaic() pe"
  tablename <- "table - alkyl chain predicted pe.csv"
}else{
  stop("Error: variable 'pred' must be set to 'Eh' or 'pe'.")
}

source("scripts//lineplot - IPL speciation vs redox.r") # load plotting script
source("scripts//barplot - IPL monte carlo speciation vs redox.r") # load plotting script

# Geochemical conditions of hot spring sites, styled as CHNOSZ input
my_basis <- c("H2O", "HCO3-", "NH3", "pH", "e-")

toplot <- c("BP1", "BP2", "BP3", "BP4", "BP5", "BP6",
            "MS1", "MS2", "MS3", "MS4", "MS5",
            "EP1", "EP3", "EP4", "EP5",
            "OS1", "OS2")


# initialize an empty rds to store calculated Eh values after each monte-carlo randomization
calc_Eh_rds_name = paste0("monte_calc_", pred, ".rds")
calc_Eh_list <- list()
for(spring in toplot){
  calc_Eh_list[[spring]][["actual"]] <- numeric(0)
  for(y in paste0("y", seq(1, length(toplot)))){
    calc_Eh_list[[spring]][[y]] <- numeric(0)
  }
}

saveRDS(calc_Eh_list, calc_Eh_rds_name)

redox_predicted_list <- c()
spring_list <- c()

for (spring in toplot){
  source <- substr(spring, start=1, stop=2)
  x_rng <- calcrng[[pred]][[source]][1:2]
  by <- calcrng[[pred]][[source]][3]
  my_species <- calcrng$my_species[[source]] # representative lipid indices

  x_rng_plot <- pltrng[[pred]][[source]][1:2]
  by_plot <- pltrng[[pred]][[source]][3]

  # # test
  # x_rng_1 <- -13
  # x_rng_2 <- 0
  # by <- 1
  # res <- res*2
  # x_rng <- c(x_rng_1, x_rng_2)



  my_activities <- c(0, df[spring, "logCO2"], df[spring, "logNH3"], df[spring, "pH"], 0)
  my_T <- df[spring, "T"]
  my_pH <- df[spring, "pH"]

  ox_waterline <- water_stability(T=as.numeric(my_T), P=1, pH=as.numeric(my_pH), units=pred, which="oxidizing")
  red_waterline <- water_stability(T=as.numeric(my_T), P=1, pH=as.numeric(my_pH), units=pred, which="reducing")
  # print(paste("oxidized_waterline:", ox_waterline))
  # print(paste("reduced_waterline:", red_waterline))

  # print(paste("Regular x_range:", x_rng - red_waterline))

  if(spring == "MS5"){
    my_basis <- c("H2O", "HCO3-", "NO3-", "pH", "e-") # NH4+ bdl, using NO3- instead
    my_activities <- c(0, df[spring, "logCO2"], df[spring, "logNO3"], df[spring, "pH"], 0)
  }


  # create an OBIGT-style .csv that can be read by CHNOSZ and a thermo_results object file
  IPL_thermo(IPL_master, "mergedIPL 1", IPLthermo_directory,
    my_basis=my_basis, my_activities=my_activities, my_species=my_species,
    my_T=my_T, my_balance=1, res=res, x_rng=x_rng,
    save_object=TRUE, thermo_method=thermo_method,
    my_sample=spring, calc_Eh=TRUE, create_pub_table=TRUE, monte=FALSE,
    ox_waterline=ox_waterline, red_waterline=red_waterline)


  print(paste("Creating line plot for", spring))
  redox_predicted <- create_lineplot_pe(spring, min=x_rng_plot[1], max=x_rng_plot[2], by=by_plot)
  redox_predicted_list <- c(redox_predicted_list, redox_predicted)
  spring_list <- c(spring_list, spring)

  # Create csv of alkyl chain predicted redox
  # for pub, use res 800
  if(spring == toplot[length(toplot)]){
      tb <- data.frame(sample=spring_list)
      tb <- cbind(tb, redox_predicted_list)
      names(tb)[ncol(tb)] <- paste0("Lip_", pred)
      write.table(tb, tablename, sep=",", row.names=FALSE)
  }


  ### MONTE CARLO ###

  if(!full_random){
    # perform monte-carlo variation of peak areas
    IPL_master_monte <- IPL_monte(IPL_master, IPLworkbook_directory, seed=seed,
                                  monte_iter=monte_iter, peak_vary=0.3, RF_vary=100,
                                  full_random=F)
  }else{
    IPL_master_monte <- IPL_monte(IPL_master, IPLworkbook_directory, seed=seed,
                                  monte_iter=monte_iter, peak_vary=0.3, RF_vary=1,
                                  full_random=T)
  }

  # # test
  mc_x_rng_1 <- red_waterline # using water stability region for x_range
  mc_x_rng_2 <- ox_waterline # using water stability region for x_range
  mc_by <- 0.02
  mc_res <- res*2

  # mc_x_rng_1 <- x_rng[1]
  # mc_x_rng_2 <- x_rng[2]
  # mc_by <- by
  # mc_res <- res

  #print(paste("Monte carlo x_range:", mc_x_rng_1 - red_waterline, mc_x_rng_2 - red_waterline))

  # initialize empty data frame that will store thermo results as they are generated
  iter_results <- data.frame()

  # calculate thermodynamic properties for each iteration of IPL_master_monte
  thermo_time <- proc.time()
  for (it in 1:length(IPL_master_monte)){
    # create an OBIGT-style .csv that can be read by CHNOSZ and a thermo_results object file
    thermo_result <- IPL_thermo(IPL_master_monte, paste(it),
      IPLthermo_directory, my_basis=my_basis,
        my_activities=my_activities, my_species=my_species,
        my_T=my_T, my_balance=1, res=mc_res, x_rng=c(mc_x_rng_1, mc_x_rng_2),
        save_object=FALSE, thermo_method=thermo_method, my_sample=spring, 
        calc_Eh=TRUE, create_pub_table=FALSE,
        save_calc_Eh_to_rds=TRUE, calc_Eh_rds_name=calc_Eh_rds_name, monte=TRUE,
        ox_waterline=ox_waterline, red_waterline=red_waterline)

    iter_results <- rbind(iter_results, thermo_result)

  }

  # save results
  saveRDS(iter_results, paste0("rds//monte//IPL_thermo_peak_area_RF_monte_", spring, ".rds"))
  saveRDS(IPL_master_monte, "rds//monte//IPL_master_monte_area_RF.rds")

  print(paste("finished monte carlo iterations for", spring))
  print(proc.time() - thermo_time)

  # # create barplot
  # create_mc_barplot(spring, iter=monte_iter, min=mc_x_rng_1, max=mc_x_rng_2, by=mc_by)
  # print(paste("Barplot created for", spring))
  

} # end for(spring in toplot)

