# function to get p-value of overall regression model
lmp <- function (modelobject) {
    if (class(modelobject) != "lm") stop("Not an object of class 'lm' ")
    f <- summary(modelobject)$fstatistic
    p <- pf(f[1], f[2], f[3], lower.tail=F)
    attributes(p) <- NULL
    return(p)
}

# function to add error bars to alkyl redox plots
# x_var e.g., "TempC"
# y_var e.g., "Lip_pe"
errbar <- function (x_var, y_var) {
  for(site in names(t)){
    this_mean <- mean(t[[site]][["actual"]])
    bar_len <- sd(t[[site]][["actual"]])
    arrows(x0=df[site, x_var], y0=this_mean-bar_len, y1=this_mean+bar_len, length=0.02, angle=90, code=3)
  }
}

# disable error bars
# errbar <- function (x_var, y_var) {}

# load IPL_master
IPL_master <- readRDS("rds//IPL_master.rds")

t <- readRDS(paste0("monte_calc_", pred, ".rds"))

metric <- pred

if(metric=="Eh"){
  lab <- "Eh, volts"
  this_col <- "Lip_Eh"
  this_file <- "lip_Eh.csv"
  # define y-axis range of lipid-predicted Eh values
  ymin <- -0.7
  ymax <- -0.2
  yby  <- 0.1
  yticks<- seq(ymin, ymax, by=yby)
}else if(metric=="pe"){
  lab <- "pe"
  this_col <- "Lip_pe"
  this_file <- "lip_pe.csv"
  ymin <- -10
  ymax <- -4
  yby  <- 0.5
  yticks<- seq(ymin, ymax, by=yby)
}else{
  stop("Error in when creating scatterplot: metric must be either 'Eh' or 'pe'.")
}

# load environmental and lipid-predicted redox values
df <- as.data.frame(read.csv(this_file, row.names = 1, check.names=F))
df <- apply(df, MARGIN = 1:2, FUN = as.numeric)

# define symbols
pch_bison_filled = 19 # Black circle
pch_mound_filled = 17 # Black triangle
pch_empress_filled = 15 # Black square
pch_octopus_filled = 18 # Black diamond

# define symbol size
pch_size_legend = c(1.5, 1.5, 1.5, 1.5*1.25)
pch_size = 1.5

# define label size
axis_lab = 1.1
tick_lab = 1.1

# y-axis label
y_lab = paste("Alkyl chain-predicted", lab)

# png(file=paste0("plots//scatterplot - alkyl chain predicted ", metric, ".png"), width=6.5, height=8.5, units = 'in', res = 300) # create png
pdf(file=paste0("plots//scatterplot - alkyl chain predicted ", metric, ".pdf"), width = 6.5, height = 8.5) # create pdf
par(mfrow = c(3, 2), mar = c(4.1, 4.1, 1.1, 1.1))

# temperature plot
xmin <- 20
xmax <- 100
xby  <- 10
plot(0, xlim = c(xmin, xmax), ylim = c(ymin, ymax), cex.lab=axis_lab,
  xaxt="n", yaxt="n",
  ylab = y_lab,
  xlab = expression(paste("Temperature, ", degree, "C", sep = "")))
errbar("TempC", this_col)
if (length(grep("BP", rownames(df)) > 0)){
  these_rows <- grep("BP", rownames(df))
  points(df[these_rows, "TempC"], df[these_rows, this_col], pch = pch_bison_filled, cex = pch_size)
}
if (length(grep("MS", rownames(df)) > 0)){
  these_rows <- grep("MS", rownames(df))
  points(df[these_rows, "TempC"], df[these_rows, this_col], pch = pch_mound_filled, cex = pch_size)
}
if (length(grep("EP", rownames(df)) > 0)){
  these_rows <- grep("EP", rownames(df))
  points(df[these_rows, "TempC"], df[these_rows, this_col], pch = pch_empress_filled, cex = pch_size)
}
if (length(grep("OS", rownames(df)) > 0)){
  these_rows <- grep("OS", rownames(df))
  points(df[these_rows, "TempC"], df[these_rows, this_col], pch = pch_octopus_filled, cex = pch_size*1.25)
}
# legend("bottomleft", legend = c("BHP polar headgroup predicted Eh ", "IPL alkyl chain predicted Eh"), pch = c(19, 1))
legend("bottomleft", legend = c("Bison Pool", "Mound Spring", "Empress Pool", "Octopus Spring"), pch = c(pch_bison_filled, pch_mound_filled, pch_empress_filled, pch_octopus_filled), pt.cex = pch_size_legend)

abline(lm(df[, this_col] ~ df[, "TempC"]))
# R2 labels
IPL_lm_TempC <- lm(df[, this_col] ~ df[, "TempC"])
IPL_slope_TempC <- formatC(IPL_lm_TempC$coefficients[2], digits = 2, format = "e")
IPL_lm_p_TempC <- formatC(lmp(IPL_lm_TempC), digits = 2, format = "e")
IPL_slope_SE_TempC <- formatC(summary(IPL_lm_TempC)$coefficients[,2][2], digits = 2, format = "e")
IPL_intercept_TempC <- format(round(IPL_lm_TempC$coefficients[1], 2), nsmall = 2)
IPL_intercept_SE_TempC <- format(round(summary(IPL_lm_TempC)$coefficients[,2][1], 2), nsmall = 2)
IPL_R2_TempC <- format(round(summary(IPL_lm_TempC)$r.squared, 2), nsmall = 2)
IPL_label_TempC <- paste("y = ", IPL_slope_TempC, "(\u00B1", IPL_slope_SE_TempC, ")x", IPL_intercept_TempC, "(\u00B1", IPL_intercept_SE_TempC, "), ", sep="")
IPL_label_TempC <- as.expression(bquote(.(IPL_label_TempC) ~ italic(R^{2}) == .(IPL_R2_TempC)))
sa_label1_TempC <- paste0("slope: ", IPL_slope_TempC, " \u00B1 ", IPL_slope_SE_TempC, "\nintercept: ", IPL_intercept_TempC, " \u00B1 ", IPL_intercept_SE_TempC, "\np-value: ", IPL_lm_p_TempC)
sa_label2_TempC <- bquote(R^2 ~ .(IPL_R2_TempC))
legend("topleft",
  legend = do.call('expression', list(sa_label1_TempC, sa_label2_TempC)), # do.call is necessary to combine output from paste() and bquote() or expression() in the same legend
  adj = c(0, 3),
  bty="n",
  xjust = 0, yjust = 0,
  xpd=TRUE)
xticks<- seq(xmin, xmax, by=xby)
axis(1, at=xticks, labels=xticks, cex.axis=tick_lab)
axis(2, at=yticks, labels=yticks, cex.axis=tick_lab)

# pH plot
xmin <- 5
xmax <- 10
xby  <- 0.5
xby_lab <- 1
plot(0, xlim = c(xmin, xmax), ylim = c(ymin, ymax), cex.lab=axis_lab,
  xaxt="n", yaxt="n",
  ylab = y_lab,
  xlab = "pH")
errbar("pH", this_col)
if (length(grep("BP", rownames(df)) > 0)){
  these_rows <- grep("BP", rownames(df))
  points(df[these_rows, "pH"], df[these_rows, this_col], pch = pch_bison_filled, cex = pch_size)
}
if (length(grep("MS", rownames(df)) > 0)){
  these_rows <- grep("MS", rownames(df))
  points(df[these_rows, "pH"], df[these_rows, this_col], pch = pch_mound_filled, cex = pch_size)
}
if (length(grep("EP", rownames(df)) > 0)){
  these_rows <- grep("EP", rownames(df))
  points(df[these_rows, "pH"], df[these_rows, this_col], pch = pch_empress_filled, cex = pch_size)
}
if (length(grep("OS", rownames(df)) > 0)){
  these_rows <- grep("OS", rownames(df))
  points(df[these_rows, "pH"], df[these_rows, this_col], pch = pch_octopus_filled, cex = pch_size*1.25)
}
abline(lm(df[, this_col] ~ df[, "pH"]))
# R2 labels
IPL_lm_TempC <- lm(df[, this_col] ~ df[, "pH"])
IPL_slope_TempC <- formatC(IPL_lm_TempC$coefficients[2], digits = 2, format = "e")
IPL_lm_p_TempC <- formatC(lmp(IPL_lm_TempC), digits = 2, format = "e")
IPL_slope_SE_TempC <- formatC(summary(IPL_lm_TempC)$coefficients[,2][2], digits = 2, format = "e")
IPL_intercept_TempC <- format(round(IPL_lm_TempC$coefficients[1], 2), nsmall = 2)
IPL_intercept_SE_TempC <- format(round(summary(IPL_lm_TempC)$coefficients[,2][1], 2), nsmall = 2)
IPL_R2_TempC <- format(round(summary(IPL_lm_TempC)$r.squared, 2), nsmall = 2)
IPL_label_TempC <- paste("y = ", IPL_slope_TempC, "(\u00B1", IPL_slope_SE_TempC, ")x", IPL_intercept_TempC, "(\u00B1", IPL_intercept_SE_TempC, "), ", sep="")
IPL_label_TempC <- as.expression(bquote(.(IPL_label_TempC) ~ italic(R^{2}) == .(IPL_R2_TempC)))
sa_label1_TempC <- paste0("slope: ", IPL_slope_TempC, " \u00B1 ", IPL_slope_SE_TempC, "\nintercept: ", IPL_intercept_TempC, " \u00B1 ", IPL_intercept_SE_TempC, "\np-value: ", IPL_lm_p_TempC)
sa_label2_TempC <- bquote(R^2 ~ .(IPL_R2_TempC))
if(metric == "Eh"){
  legend("topleft",
    legend = do.call('expression', list(sa_label1_TempC, sa_label2_TempC)), # do.call is necessary to combine output from paste() and bquote() or expression() in the same legend
    adj = c(0, 3),
    bty="n",
    xjust = 0, yjust = 0,
    xpd=TRUE)
}else{
  legend("bottomleft",
    legend = do.call('expression', list(sa_label1_TempC, sa_label2_TempC)), # do.call is necessary to combine output from paste() and bquote() or expression() in the same legend
    #adj = c(0, 3),
    bty="n",
    xjust = 0, yjust = 0,
    xpd=TRUE)
}
xticks_lab<- seq(xmin, xmax, by=xby_lab)
axis(1, at=xticks, labels=FALSE)
axis(1, at=xticks_lab, labels=xticks_lab, cex.axis=tick_lab)
axis(2, at=yticks, labels=yticks, cex.axis=tick_lab)

# Redox (O2/H2O) plot
if(metric == "Eh"){
  xmin <- 0.5
  xmax <- 0.8
  xby  <- 0.05
  xby_lab <- 0.1
}else{
  xmin <- 6
  xmax <- 13
  xby  <- 0.5
  xby_lab <- 1
}
plot(0, xlim = c(xmin, xmax), ylim = c(ymin, ymax), cex.lab=axis_lab,
    xaxt="n", yaxt="n",
  ylab = y_lab,
  xlab = expression("O"[2]*"/H"[2]*"O"))
errbar("O2_AQ/H2O", this_col)
if (length(grep("BP", rownames(df)) > 0)){
  these_rows <- grep("BP", rownames(df))
  points(df[these_rows, "O2_AQ/H2O"], df[these_rows, this_col], pch = pch_bison_filled, cex = pch_size)
}
if (length(grep("MS", rownames(df)) > 0)){
  these_rows <- grep("MS", rownames(df))
  points(df[these_rows, "O2_AQ/H2O"], df[these_rows, this_col], pch = pch_mound_filled, cex = pch_size)
}
if (length(grep("EP", rownames(df)) > 0)){
  these_rows <- grep("EP", rownames(df))
  points(df[these_rows, "O2_AQ/H2O"], df[these_rows, this_col], pch = pch_empress_filled, cex = pch_size)
}
if (length(grep("OS", rownames(df)) > 0)){
  these_rows <- grep("OS", rownames(df))
  points(df[these_rows, "O2_AQ/H2O"], df[these_rows, this_col], pch = pch_octopus_filled, cex = pch_size*1.25)
}
# legend("bottomright", legend = c("BHP polar headgroup predicted Eh ", "IPL alkyl chain predicted Eh"), pch = c(19, 1))
abline(lm(df[, this_col] ~ df[, "O2_AQ/H2O"]))
# equation and R2 labels
IPL_lm <- lm(df[, this_col] ~ df[, "O2_AQ/H2O"])
IPL_lm_p <- formatC(lmp(IPL_lm), digits = 2, format = "e")
IPL_slope <- format(round(IPL_lm$coefficients[2], 2), nsmall = 2)
IPL_slope_SE <- format(round(summary(IPL_lm)$coefficients[,2][2], 2), nsmall = 2)
IPL_intercept <- format(round(IPL_lm$coefficients[1], 2), nsmall = 2)
IPL_intercept_SE <- format(round(summary(IPL_lm)$coefficients[,2][1], 2), nsmall = 2)
IPL_R2 <- format(round(summary(IPL_lm)$r.squared, 2), nsmall = 2)
IPL_label <- paste("y = ", IPL_slope, "(\u00B1", IPL_slope_SE, ")x", IPL_intercept, "(\u00B1", IPL_intercept_SE, "), ", sep="")
IPL_label <- as.expression(bquote(.(IPL_label) ~ italic(R^{2}) == .(IPL_R2)))
# text(0.60, -0.60, IPL_label, pos = 4)
sa_label1 <- paste0("slope: ", IPL_slope, " \u00B1 ", IPL_slope_SE, "\nintercept: ", IPL_intercept, " \u00B1 ", IPL_intercept_SE, "\np-value: ", IPL_lm_p)
sa_label2 <- bquote(R^2 ~ .(IPL_R2))
legend("topleft",
  legend = do.call('expression', list(sa_label1, sa_label2)), # do.call is necessary to combine output from paste() and bquote() or expression() in the same legend
  adj = c(0, 3),
  bty="n",
  xjust = 0, yjust = 0,
  xpd=TRUE)
xticks<- seq(xmin, xmax, by=xby)
xticks_lab<- seq(xmin, xmax, by=xby_lab)
axis(1, at=xticks, labels=FALSE)
axis(1, at=xticks_lab, labels=xticks_lab, cex.axis=tick_lab)
axis(2, at=yticks, labels=yticks, cex.axis=tick_lab)


# Redox (NO2/NO3) plot
if(metric == "Eh"){
  xmin <- 0.20
  xmax <- 0.40
  xby  <- 0.05
  x_by_lab <- 0.1
}else{
  xmin <- 3
  xmax <- 6
  xby  <- 0.5
  x_by_lab <- 1
}
plot(0, xlim = c(xmin, xmax), ylim = c(ymin, ymax), cex.lab=axis_lab,
    xaxt="n", yaxt="n",
    ylab = y_lab,
    xlab = expression("NO"[2]^"-"*"/NO"[3]^"-"))
errbar("NO2-/NO3-", this_col)
if (length(grep("BP", rownames(df)) > 0)){
  these_rows <- grep("BP", rownames(df))
  points(df[these_rows, "NO2-/NO3-"], df[these_rows, this_col], pch = pch_bison_filled, cex = pch_size)
}
if (length(grep("MS", rownames(df)) > 0)){
  these_rows <- grep("MS", rownames(df))
  points(df[these_rows, "NO2-/NO3-"], df[these_rows, this_col], pch = pch_mound_filled, cex = pch_size)
}
if (length(grep("EP", rownames(df)) > 0)){
  these_rows <- grep("EP", rownames(df))
  points(df[these_rows, "NO2-/NO3-"], df[these_rows, this_col], pch = pch_empress_filled, cex = pch_size)
}
if (length(grep("OS", rownames(df)) > 0)){
  these_rows <- grep("OS", rownames(df))
  points(df[these_rows, "NO2-/NO3-"], df[these_rows, this_col], pch = pch_octopus_filled, cex = pch_size*1.25)
}
# legend("bottomright", legend = c("BHP polar headgroup predicted Eh ", "IPL alkyl chain predicted Eh"), pch = c(19, 1))
abline(lm(df[, this_col] ~ df[, "NO2-/NO3-"]))
# equation and R2 labels
IPL_lm <- lm(df[, this_col] ~ df[, "NO2-/NO3-"])
IPL_lm_p <- formatC(lmp(IPL_lm), digits = 2, format = "e")
IPL_slope <- format(round(IPL_lm$coefficients[2], 2), nsmall = 2)
IPL_slope_SE <- format(round(summary(IPL_lm)$coefficients[,2][2], 2), nsmall = 2)
IPL_intercept <- format(round(IPL_lm$coefficients[1], 2), nsmall = 2)
IPL_intercept_SE <- format(round(summary(IPL_lm)$coefficients[,2][1], 2), nsmall = 2)
IPL_R2 <- format(round(summary(IPL_lm)$r.squared, 2), nsmall = 2)
IPL_label <- paste("y = ", IPL_slope, "(\u00B1", IPL_slope_SE, ")x", IPL_intercept, "(\u00B1", IPL_intercept_SE, "), ", sep="")
IPL_label <- as.expression(bquote(.(IPL_label) ~ italic(R^{2}) == .(IPL_R2)))
# text(0.60, -0.60, IPL_label, pos = 4)
sa_label1 <- paste0("slope: ", IPL_slope, " \u00B1 ", IPL_slope_SE, "\nintercept: ", IPL_intercept, " \u00B1 ", IPL_intercept_SE, "\np-value: ", IPL_lm_p)
sa_label2 <- bquote(R^2 ~ .(IPL_R2))
legend("topleft",
  legend = do.call('expression', list(sa_label1, sa_label2)), # do.call is necessary to combine output from paste() and bquote() or expression() in the same legend
  adj = c(0, 3),
  bty="n",
  xjust = 0, yjust = 0,
  xpd=TRUE)
xticks<- seq(xmin, xmax, by=xby)
xticks_lab<- seq(xmin, xmax, by=xby_lab)
axis(1, at=xticks, labels=FALSE)
axis(1, at=xticks_lab, labels=xticks_lab, cex.axis=tick_lab)
axis(2, at=yticks, labels=yticks, cex.axis=tick_lab)


# Redox (NH4/NO3) plot
if(metric == "Eh"){
  xmin <- 0.0
  xmax <- 0.4
  xby  <- 0.05
  xby_lab <- 0.1
}else{
  xmin <- 0
  xmax <- 6
  xby  <- 0.5
  xby_lab <- 1
}
plot(0, xlim = c(xmin, xmax), ylim = c(ymin, ymax), cex.lab=axis_lab,
  xaxt="n", yaxt="n",
  ylab = y_lab,
  xlab = expression("NH"[4]^"+"*"/NO"[3]^"-"))
errbar("NH4+/NO3-", this_col)
if (length(grep("BP", rownames(df)) > 0)){
  these_rows <- grep("BP", rownames(df))
  points(df[these_rows, "NH4+/NO3-"], df[these_rows, this_col], pch = pch_bison_filled, cex = pch_size)
}
if (length(grep("MS", rownames(df)) > 0)){
  these_rows <- grep("MS", rownames(df))
  points(df[these_rows, "NH4+/NO3-"], df[these_rows, this_col], pch = pch_mound_filled, cex = pch_size)
}
if (length(grep("EP", rownames(df)) > 0)){
  these_rows <- grep("EP", rownames(df))
  points(df[these_rows, "NH4+/NO3-"], df[these_rows, this_col], pch = pch_empress_filled, cex = pch_size)
}
if (length(grep("OS", rownames(df)) > 0)){
  these_rows <- grep("OS", rownames(df))
  points(df[these_rows, "NH4+/NO3-"], df[these_rows, this_col], pch = pch_octopus_filled, cex = pch_size*1.25)
}
abline(lm(df[, this_col] ~ df[, "NH4+/NO3-"]))
IPL_lm <- lm(df[, this_col] ~ df[, "NH4+/NO3-"])
IPL_lm_p <- formatC(lmp(IPL_lm), digits = 2, format = "e")
IPL_slope <- format(round(IPL_lm$coefficients[2], 2), nsmall = 2)
IPL_slope_SE <- format(round(summary(IPL_lm)$coefficients[,2][2], 2), nsmall = 2)
IPL_intercept <- format(round(IPL_lm$coefficients[1], 2), nsmall = 2)
IPL_intercept_SE <- format(round(summary(IPL_lm)$coefficients[,2][1], 2), nsmall = 2)
IPL_R2 <- format(round(summary(IPL_lm)$r.squared, 2), nsmall = 2)
IPL_label <- paste("y = ", IPL_slope, "(\u00B1", IPL_slope_SE, ")x", IPL_intercept, "(\u00B1", IPL_intercept_SE, "), ", sep="")
IPL_label <- as.expression(bquote(.(IPL_label) ~ italic(R^{2}) == .(IPL_R2)))
# text(0.60, -0.60, IPL_label, pos = 4)

sa_label1 <- paste0("slope: ", IPL_slope, " \u00B1 ", IPL_slope_SE, "\nintercept: ", IPL_intercept, " \u00B1 ", IPL_intercept_SE, "\np-value: ", IPL_lm_p)
sa_label2 <- bquote(R^2 ~ .(IPL_R2))
legend("topleft",
  legend = do.call('expression', list(sa_label1, sa_label2)), # do.call is necessary to combine output from paste() and bquote() or expression() in the same legend
  adj = c(0, 3),
  bty="n",
  xjust = 0, yjust = 0,
  xpd=TRUE)
xticks<- seq(xmin, xmax, by=xby)
xticks_lab<- seq(xmin, xmax, by=xby_lab)
axis(1, at=xticks, labels=FALSE)
axis(1, at=xticks_lab, labels=xticks_lab, cex.axis=tick_lab)
axis(2, at=yticks, labels=yticks, cex.axis=tick_lab)

# Redox (HS/SO4) plot
if(metric == "Eh"){
  xmin <- -0.5
  xmax <- -0.1
  xby  <- 0.05
  xby_lab  <- 0.1
}else{
  xmin <- -7
  xmax <- -2
  xby  <- 0.5
  xby_lab  <- 1
}
plot(0, xlim = c(xmin, xmax), ylim = c(ymin, ymax), cex.lab=axis_lab,
  xaxt="n", yaxt="n",
  ylab = y_lab,
  xlab = expression("HS"^"-"*"/SO"[4]^"2-"))
errbar("HS-/SO4-2", this_col)
if (length(grep("BP", rownames(df)) > 0)){
  these_rows <- grep("BP", rownames(df))
  points(df[these_rows, "HS-/SO4-2"], df[these_rows, this_col], pch = pch_bison_filled, cex = pch_size)
}
if (length(grep("MS", rownames(df)) > 0)){
  these_rows <- grep("MS", rownames(df))
  points(df[these_rows, "HS-/SO4-2"], df[these_rows, this_col], pch = pch_mound_filled, cex = pch_size)
}
if (length(grep("EP", rownames(df)) > 0)){
  these_rows <- grep("EP", rownames(df))
  points(df[these_rows, "HS-/SO4-2"], df[these_rows, this_col], pch = pch_empress_filled, cex = pch_size)
}
if (length(grep("OS", rownames(df)) > 0)){
  these_rows <- grep("OS", rownames(df))
  points(df[these_rows, "HS-/SO4-2"], df[these_rows, this_col], pch = pch_octopus_filled, cex = pch_size*1.25)
}
abline(lm(df[, this_col] ~ df[, "HS-/SO4-2"]))
IPL_lm <- lm(df[, this_col] ~ df[, "HS-/SO4-2"])
IPL_lm_p <- formatC(lmp(IPL_lm), digits = 2, format = "e")
IPL_slope <- format(round(IPL_lm$coefficients[2], 2), nsmall = 2)
IPL_slope_SE <- format(round(summary(IPL_lm)$coefficients[,2][2], 2), nsmall = 2)
IPL_intercept <- format(round(IPL_lm$coefficients[1], 2), nsmall = 2)
IPL_intercept_SE <- format(round(summary(IPL_lm)$coefficients[,2][1], 2), nsmall = 2)
IPL_R2 <- format(round(summary(IPL_lm)$r.squared, 2), nsmall = 2)
IPL_label <- paste("y = ", IPL_slope, "(\u00B1", IPL_slope_SE, ")x", IPL_intercept, "(\u00B1", IPL_intercept_SE, "), ", sep="")
IPL_label <- as.expression(bquote(.(IPL_label) ~ italic(R^{2}) == .(IPL_R2)))
# text(0.60, -0.60, IPL_label, pos = 4)

sa_label1 <- paste0("slope: ", IPL_slope, " \u00B1 ", IPL_slope_SE, "\nintercept: ", IPL_intercept, " \u00B1 ", IPL_intercept_SE, "\np-value: ", IPL_lm_p)
sa_label2 <- bquote(R^2 ~ .(IPL_R2))
legend("topleft",
  legend = do.call('expression', list(sa_label1, sa_label2)), # do.call is necessary to combine output from paste() and bquote() or expression() in the same legend
  adj = c(0, 3),
  bty="n",
  xjust = 0, yjust = 0,
  xpd=TRUE)
xticks<- seq(xmin, xmax, by=xby)
xticks_lab<- seq(xmin, xmax, by=xby_lab)
axis(1, at=xticks, labels=FALSE)
axis(1, at=xticks_lab, labels=xticks_lab, cex.axis=tick_lab)
axis(2, at=yticks, labels=yticks, cex.axis=tick_lab)

dev.off()
