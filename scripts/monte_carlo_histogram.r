# calculate the proportion of iterations with R values greater than the "canon" R value.
# similar to a p-value in a Mantel test.


if(pred=="Eh"){
  this_col <- "Lip_Eh"
  this_file <- "lip_Eh.csv"
}else if(pred=="pe"){
  this_col <- "Lip_pe"
  this_file <- "lip_pe.csv"
}

# load environmental and lipid-predicted redox values
df <- as.data.frame(read.csv(this_file, row.names = 1, check.names=F))
df <- apply(df, MARGIN = 1:2, FUN = as.numeric)

df <- df[!(row.names(df) %in% "EP2"), ]

# load alkyl-predicted redox values from monte carlo iterations
t <- readRDS(paste0("monte_calc_", pred, ".rds"))

for(var in colnames(df)[1:length(colnames(df))-1]){
  r_vals <- c()
  for(i in 1:length(t[["BP1"]][["actual"]])){
    alkyl_preds <- c()
    for(site in names(t)){
      alkyl_preds <- c(alkyl_preds, t[[site]][["actual"]][i])
    }
    r <- cor(x=df[, var], y=alkyl_preds, method='pearson', use="complete.obs")
    r_vals <- c(r_vals, r)
  }
  r_actual <- cor(x=df[, var], y=df[, this_col], method='pearson', use="complete.obs")
  r_vals_sig <- r_vals[r_vals^2 >= r_actual^2]
  print(paste(var, "r-squared p:", length(r_vals_sig)/length(r_vals)))

  # plot pearson r histograms
  hist(r_vals, main=var, xlab="r", ylab="frequency", xlim=c(-1, 1))
  abline(v=r_actual, lwd=2, col="red")

  # plot r2 histograms
  hist(r_vals^2, main=var, xlab="r squared", ylab="frequency", xlim=c(0, 1))
  abline(v=r_actual^2, lwd=2, col="red")
}
