library(scales)
library(lattice)
library(readxl)


# function to output a chemical formula from elemental & charge composition
lipid_formula <- function(C_cc, H_cc, N_cc, O_cc, P_cc, Zplus_cc, Zminus_cc, S_cc){
  default_scipen_setting <- getOption("scipen")
  options(scipen = 999)
  formula <- paste(
    if(C_cc > 0){paste("C", if(C_cc != 1){C_cc}, sep="")},
    if(H_cc > 0){paste("H", if(H_cc != 1){H_cc}, sep="")},
    if(N_cc > 0){paste("N", if(N_cc != 1){N_cc}, sep="")},
    if(O_cc > 0){paste("O", if(O_cc != 1){O_cc}, sep="")},
    if(P_cc > 0){paste("P", if(P_cc != 1){P_cc}, sep="")},
    if(S_cc > 0){paste("S", if(S_cc != 1){S_cc}, sep="")},
    if(Zplus_cc > 0){paste("+", if(Zplus_cc != 1){Zplus_cc}, sep="")},
    if(Zminus_cc > 0){paste("-", if(Zminus_cc != 1){Zminus_cc}, sep="")},
    sep="")
  options(scipen = default_scipen_setting)
  return(formula)
}

# function to output a chemical formula from elemental & charge composition, formatted for LaTeX
lipid_formula_LaTeX <- function(C_cc, H_cc, N_cc, O_cc, P_cc, Zplus_cc, Zminus_cc, S_cc){
  # default_scipen_setting <- getOption("scipen")
  # options(scipen = 0)

  args <- c(C_cc, H_cc, N_cc, O_cc, P_cc, Zplus_cc, Zminus_cc, S_cc)
  new_args <- unlist(lapply(args, FUN=formatval))
  C_cc_formatted <- new_args[1]
  H_cc_formatted <- new_args[2]
  N_cc_formatted <- new_args[3]
  O_cc_formatted <- new_args[4]
  P_cc_formatted <- new_args[5]
  Zplus_cc_formatted <- new_args[6]
  Zminus_cc_formatted <- new_args[7]
  S_cc_formatted <- new_args[8]

  formula <- paste0(
    if(C_cc > 0){paste0("C", if(C_cc != 1){paste0("\\textsubscript{", C_cc_formatted, "}")})},
    if(H_cc > 0){paste0("H", if(H_cc != 1){paste0("\\textsubscript{", H_cc_formatted, "}")})},
    if(N_cc > 0){paste0("N", if(N_cc != 1){paste0("\\textsubscript{", N_cc_formatted, "}")})},
    if(O_cc > 0){paste0("O", if(O_cc != 1){paste0("\\textsubscript{", O_cc_formatted, "}")})},
    if(P_cc > 0){paste0("P", if(P_cc != 1){paste0("\\textsubscript{", P_cc_formatted, "}")})},
    if(S_cc > 0){paste0("S", if(S_cc != 1){paste0("\\textsubscript{", S_cc_formatted, "}")})},
    if(Zplus_cc > 0){paste0(if(Zplus_cc != 1){paste0("\\textsuperscript{+", Zplus_cc_formatted, "}")})},
    if(Zminus_cc > 0){paste0(if(Zminus_cc != 1){paste0("\\textsuperscript{-", Zminus_cc_formatted, "}")})})

  # formula <- gsub("e\\+00", "", formula) # remove e notation
  # formula <- gsub("e\\-00", "", formula) # remove e notation
  # formula <- gsub("e\\-0", "e\\-", formula) # remove leading zero from e notation
  # formula <- gsub("e\\+0", "e\\+", formula) # remove leading zero from e notation
  return(formula)
}

formatval <- function(arg){
  if(arg > 1 & arg < 1000){
    new_arg <- formatC(signif(arg, digits = 3), digits = 3, format = "fg", flag = "#")
  } else {
    new_arg <- formatC(arg, , format = "e", digits = 2)
    # new_arg <- gsub("e\\+00", "", new_arg) # remove e notation
    # new_arg <- gsub("e\\-00", "", new_arg) # remove e notation
    new_arg <- gsub("e\\-0", "e\\-", new_arg) # remove leading zero from e notation
    new_arg <- gsub("e\\+0", "e\\+", new_arg) # remove leading zero from e notation
  }
  return(new_arg)
}

# Extract p-value from a linear model
# Code from Steven Turner:
# http://www.gettinggeneticsdone.com/2011/01/rstats-function-for-extracting-f-test-p.html
lmp <- function (modelobject) {
  if (class(modelobject) != "lm") stop("Not an object of class 'lm' ")
  f <- summary(modelobject)$fstatistic
  p <- pf(f[1],f[2],f[3],lower.tail=F)
  attributes(p) <- NULL
  return(p)
}

# scale a value 0 to 1 based on a range
scale01 <- function(x_in, x) {
  (x_in - min(x))/(max(x) - min(x))
}


# Scale a value 0.1 to 1 based on a range.
# Used for scaling transparency so that minimum values are still visible.
scale_alph <- function(x_in, x) {
  ((x_in - min(x))/(max(x) - min(x))) * 0.9 + 0.1
}

# Scale and round a number between 1 and the number of IPLs to display.
# Used for palette color scaling on plots
scale_palette <- function(x_in, vec_of_min_and_max, indiv_IPLs) {
  round(((x_in - min(vec_of_min_and_max)) /
    (max(vec_of_min_and_max) - min(vec_of_min_and_max))) *
    length(indiv_IPLs) + 1)
}

choose_palette <- function(palette_type, palette_length){
  if (palette_type == "rainbow"){
    rainbow(palette_length)
  } else if (palette_type == "topo") {
    topo.colors(palette_length)
  } else {
    stop("Chosen palette_type is not recognized.")
  }
}


# modified from CHNOSZ's github, but modified elements
# to include phosphorus and changed oxidation state of sulfur
ZC_lip <- function(formula) {
  # calculate average oxidation state of carbon
  # from chemical formulas of species
  # if we haven't been supplied with a stoichiometric matrix, first get the formulas
  formula <- i2A(get.formula(formula))
  # is there carbon there?
  iC <- match("C", colnames(formula))
  if(is.na(iC)) stop("carbon not found in the stoichiometric matrix")
  # the nominal charges of elements other than carbon
  # FIXME: add more elements, warn about missing ones
  knownelement <- c("H", "N", "O", "S", "P", "Z")
  charge <- c(-1, 3, 2, -4, -5, 1)
  # where are these elements in the formulas?
  iknown <- match(knownelement, colnames(formula))
  # any unknown elements in formula get dropped with a warning
  iunk <- !colnames(formula) %in% c(knownelement, "C")
  if(any(iunk)) warning(paste("element(s)",paste(colnames(formula)[iunk], collapse=" "),
    "not in", paste(knownelement, collapse=" "), "so not included in this calculation"))
  # contribution to charge only from known elements that are in the formula
  formulacharges <- t(formula[,iknown[!is.na(iknown)]]) * charge[!is.na(iknown)]
  # sum of the charges; the arrangement depends on the number of formulas
  if(nrow(formula)==1) formulacharge <- rowSums(formulacharges)
  else formulacharge <- colSums(formulacharges)
  # numbers of carbons
  nC <- formula[,iC]
  # average oxidation state
  ZC <- as.numeric(formulacharge/nC)
  return(ZC)
}

### FROM CHNOSZ ###
### unexported functions ###

# convert to integer without NA coercion warnings
as.integer.nowarn <- function(x) {
  if(length(x) == 0) integer() else
  if(length(x) > 1) sapply(x, as.integer.nowarn) else
  if(grepl("[^0-9]", x)) NA_integer_ else as.integer(x)
}

# Accept a numeric or character argument; the character argument can be mixed
#   (i.e. include quoted numbers). as.numeric is tested on every value; numeric values
#   are then interpreted as species indices in the thermodynamic database (rownumbers of thermo()$obigt),
#   and the chemical formulas for those species are returned.
# Values that can not be converted to numeric are returned as-is.
get.formula <- function(formula) {
  # return the argument if it's a matrix or named numeric
  if(is.matrix(formula)) return(formula)
  if(is.numeric(formula) & !is.null(names(formula))) return(formula)
  # return the argument as matrix if it's a data frame
  if(is.data.frame(formula)) return(as.matrix(formula))
  # return the values in the argument, or chemical formula(s)
  # for values that are species indices
  # for numeric values, get the formulas from those rownumbers of thermo()$obigt
  i <- as.integer.nowarn(formula)
  # we can't have more than the number of rows in thermo()$obigt
  thermo <- get("thermo")
  iover <- i > nrow(thermo()$obigt)
  iover[is.na(iover)] <- FALSE
  if(any(iover)) stop(paste("species number(s)",paste(i[iover],collapse=" "),
    "not available in thermo()$obigt"))
  # we let negative numbers pass as formulas
  i[i < 0] <- NA
  # replace any species indices with formulas from thermo()$obigt
  formula[!is.na(i)] <- thermo()$obigt$formula[i[!is.na(i)]]
  return(formula)
}

print("Misc. functions loaded...")
