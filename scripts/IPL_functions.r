suppressMessages({
  library(readxl)
  library(CHNOSZ)
})

#function to calculate ZC from chemical composition
lipid_ZC <- function(C_cc, H_cc, N_cc, O_cc, P_cc, Zplus_cc, Zminus_cc, S_cc, Z=NA){
  this_charge <- ifelse(is.na(Z), Zplus_cc - Zminus_cc, Z) # assumes Zminus_cc is given as a positive number
  this_ZC <- (-(H_cc) + (2 * O_cc) + (3 * N_cc) -
      (5 * P_cc) - (4 * S_cc) + this_charge) / (C_cc)
  return(this_ZC)
}

# function to output a chemical formula from elemental & charge composition
lipid_formula <- function(C_cc, H_cc, N_cc, O_cc, P_cc, Zplus_cc, Zminus_cc, S_cc){
  default_scipen_setting <- getOption("scipen")
  options(scipen = 999)
  formula <- paste(
    ifelse(C_cc > 0, paste("C", ifelse(C_cc != 1, C_cc, ""), sep=""), ""),
    ifelse(H_cc > 0, paste("H", ifelse(H_cc != 1, H_cc, ""), sep=""), ""),
    ifelse(N_cc > 0, paste("N", ifelse(N_cc != 1, N_cc, ""), sep=""), ""),
    ifelse(O_cc > 0, paste("O", ifelse(O_cc != 1, O_cc, ""), sep=""), ""),
    ifelse(P_cc > 0, paste("P", ifelse(P_cc != 1, P_cc, ""), sep=""), ""),
    ifelse(S_cc > 0, paste("S", ifelse(S_cc != 1, S_cc, ""), sep=""), ""),
    ifelse(Zplus_cc > 0, paste("+", ifelse(Zplus_cc != 1, Zplus_cc, ""), sep=""), ""),
    ifelse(Zminus_cc > 0, paste("-", ifelse(Zminus_cc != 1, Zminus_cc, ""), sep=""), ""),
    sep="")
  options(scipen = default_scipen_setting)
  return(formula)
}

# function to apply random noise to peak areas and response factors in a
# monte-carlo-style fashion
IPL_monte <- function(IPL_master, IPLworkbook_directory, seed = 190424,
  monte_iter = 0, peak_vary = 0.2, RF_vary = 0.5, full_random = FALSE){

  time <- proc.time()

  set.seed(seed)

  default_scipen_setting <- getOption("scipen") # get default setting for scientific notation
  options(scipen=999) # turn scientific notation off temporarily

  # function to remove samples that do not have peak areas for a specified IPL
  # and to turn vectors into matrices readable by IPL_thermo_func
  rmv_smpl <- function(x){
    if(is.matrix(x)){
      as.matrix(x[, colSums(IPLareasRFmol) != 0, drop = FALSE])
    } else if (is.vector(x)){
      as.matrix(x[colSums(IPLareasRFmol) != 0, drop = FALSE])
    } else {
      print(paste("Error removing samples without peak areas for IPL"))
    }
  }

  # function to report weighted formulas
  ave_formula <- function(C_ave, H_ave, O_ave, N_ave, P_ave, S_ave, Z_ave){

    ave_formula <- paste0("C", signif(C_ave, 3),
                          "H", signif(H_ave, 3),
                          "O", signif(O_ave, 3),
                          "N", signif(N_ave, 3),
                          "P", signif(P_ave, 3),
                          "S", signif(S_ave, 3),
                          "Z", signif(Z_ave, 3))

    return(ave_formula)
  }

  # functions to apply random variation to peak areas quickly
  if(!full_random){
    # randomly vary peak areas to some threshold (e.g. 30%)
    random_area <- function(x){ x + runif(1, min=x*(-peak_vary), max=x*(peak_vary))} # summing
  }else{
    # randomize peak areas between 0 and 1
    random_area <- function(x){runif(1, min=0, max=1)}
  }

  IPL_master_monte <- list()
  mipl <- list()

  # Extract information from IPL_master
  IPLareas <- IPL_master[[length(IPL_master)]][["IPLareas"]]
  masses <- IPL_master[[length(IPL_master)]][["masses"]]
  IPL_RFith <- IPL_master[[length(IPL_master)]][["IPL_RFith"]]

  # IPL_names <- IPL_master[[length(IPL_master)]][["this IPL"]]
  RF_nrows <- IPL_master[[length(IPL_master)]][["IPL_RFith nrow"]]

  n_chains_matrix <- IPL_master[[length(IPL_master)]][["n_chains_matrix"]]
  n_chains_ether_matrix <- IPL_master[[length(IPL_master)]][["n_chains_ether_matrix"]]
  n_chains_ester_matrix <- IPL_master[[length(IPL_master)]][["n_chains_ester_matrix"]]
  n_chains_amide_matrix <- IPL_master[[length(IPL_master)]][["n_chains_amide_matrix"]]
  n_chains_nonlinkage_matrix <- IPL_master[[length(IPL_master)]][["n_chains_nonlinkage_matrix"]]
  n_chains_hydroxylated_matrix <- IPL_master[[length(IPL_master)]][["n_chains_hydroxylated_matrix"]]
  n_chains_GDGT_matrix <- IPL_master[[length(IPL_master)]][["n_chains_GDGT_matrix"]]
  n_chains_AR_matrix <- IPL_master[[length(IPL_master)]][["n_chains_AR_matrix"]]

  n_heads_matrix <- IPL_master[[length(IPL_master)]][["n_heads_matrix"]]
  n_backbones_matrix <- IPL_master[[length(IPL_master)]][["n_backbones_matrix"]]

  C_all_IPL_matrix <- IPL_master[[length(IPL_master)]][["C_all_IPL_matrix"]]
  H_all_IPL_matrix <- IPL_master[[length(IPL_master)]][["H_all_IPL_matrix"]]
  N_all_IPL_matrix <- IPL_master[[length(IPL_master)]][["N_all_IPL_matrix"]]
  O_all_IPL_matrix <- IPL_master[[length(IPL_master)]][["O_all_IPL_matrix"]]
  S_all_IPL_matrix <- IPL_master[[length(IPL_master)]][["S_all_IPL_matrix"]]
  P_all_IPL_matrix <- IPL_master[[length(IPL_master)]][["P_all_IPL_matrix"]]
  Z_all_IPL_matrix <- IPL_master[[length(IPL_master)]][["Z_all_IPL_matrix"]]
  C_all_head_matrix <- IPL_master[[length(IPL_master)]][["C_all_head_matrix"]]
  H_all_head_matrix <- IPL_master[[length(IPL_master)]][["H_all_head_matrix"]]
  N_all_head_matrix <- IPL_master[[length(IPL_master)]][["N_all_head_matrix"]]
  O_all_head_matrix <- IPL_master[[length(IPL_master)]][["O_all_head_matrix"]]
  S_all_head_matrix <- IPL_master[[length(IPL_master)]][["S_all_head_matrix"]]
  P_all_head_matrix <- IPL_master[[length(IPL_master)]][["P_all_head_matrix"]]
  Z_all_head_matrix <- IPL_master[[length(IPL_master)]][["Z_all_head_matrix"]]
  C_all_backbone_matrix <- IPL_master[[length(IPL_master)]][["C_all_backbone_matrix"]]
  H_all_backbone_matrix <- IPL_master[[length(IPL_master)]][["H_all_backbone_matrix"]]
  N_all_backbone_matrix <- IPL_master[[length(IPL_master)]][["N_all_backbone_matrix"]]
  O_all_backbone_matrix <- IPL_master[[length(IPL_master)]][["O_all_backbone_matrix"]]
  S_all_backbone_matrix <- IPL_master[[length(IPL_master)]][["S_all_backbone_matrix"]]
  P_all_backbone_matrix <- IPL_master[[length(IPL_master)]][["P_all_backbone_matrix"]]
  Z_all_backbone_matrix <- IPL_master[[length(IPL_master)]][["Z_all_backbone_matrix"]]
  C_all_chain_matrix <- IPL_master[[length(IPL_master)]][["C_all_chain_matrix"]]
  H_all_chain_matrix <- IPL_master[[length(IPL_master)]][["H_all_chain_matrix"]]
  N_all_chain_matrix <- IPL_master[[length(IPL_master)]][["N_all_chain_matrix"]]
  O_all_chain_matrix <- IPL_master[[length(IPL_master)]][["O_all_chain_matrix"]]
  S_all_chain_matrix <- IPL_master[[length(IPL_master)]][["S_all_chain_matrix"]]
  P_all_chain_matrix <- IPL_master[[length(IPL_master)]][["P_all_chain_matrix"]]
  Z_all_chain_matrix <- IPL_master[[length(IPL_master)]][["Z_all_chain_matrix"]]

  nC_raw <- IPL_master[[length(IPL_master)]][["nC_raw"]]
  nUnsat_raw <- IPL_master[[length(IPL_master)]][["nUnsat_raw"]]
  IPL_nUnsatith <- IPL_master[[length(IPL_master)]][["IPL_nUnsatith"]]
  IPL_nPentRingith <- IPL_master[[length(IPL_master)]][["IPL_nPentRingith"]]
  IPL_nHexRingith <- IPL_master[[length(IPL_master)]][["IPL_nHexRingith"]]
  IPL_ZCith <- IPL_master[[length(IPL_master)]][["IPL_ZCith"]]

  # generate "rownums_for_RF", which keeps track of rows in mergedIPL that group each IPL to have a different RF
  rownums_for_RF <- 1
  for (i in 1:length(RF_nrows)){
    rownums_for_RF <- c(rownums_for_RF, RF_nrows[i-1] + rownums_for_RF[i-1])
  }
  RF_2013 <- c()
  RF_2014 <- c()
  for (row in rownums_for_RF){
    RF_2013 <- c(RF_2013, IPL_RFith[row, "Bison OF1"]^-1) # store all RFs from 2013 in a long vector
    RF_2014 <- c(RF_2014, IPL_RFith[row, "Mound OF1"]^-1) # store all RFs from 2014 in a long vector
  }

  # Begin monte carlo style iterations
  for (it in 1:monte_iter){

    IPLareas_mc <- apply(IPLareas, MARGIN = 1:2, FUN = random_area) # add random variation to peak areas
    # IPLareas_mc[IPLareas_mc < 0] <- 0 # if an area goes less than 0, replace with 0

    if(length(which(IPLareas_mc < 0)) > 0){
      stop("A peak area generated by monte carlo is negative!")
    }

    IPL_RFith_mc <- IPL_RFith/IPL_RFith # initialize IPL_RFith_mc with matrix of 1s

    rand_RF_2013 <- c()
    rand_RF_2014 <- c()
    for (i in 1:length(RF_2013)){
      rand_RF_2013[i] <- runif(1, RF_2013[i]/RF_vary, RF_2013[i]*RF_vary) # generate random 2013 RF within user-specified range
      rand_RF_2014[i] <- runif(1, RF_2014[i]/RF_vary, RF_2014[i]*RF_vary) # generate random 2014 RF within user-specified range
    }

    # Store random RFs in IPL_RFith_mc array
    for (i in 1:length(RF_2013)){
      if (i < length(RF_2013)) {
        IPL_RFith_mc[rownums_for_RF[i]:rownums_for_RF[i+1]-1, 1:14] <- rand_RF_2013[i]
        IPL_RFith_mc[rownums_for_RF[i]:rownums_for_RF[i+1]-1, 15:18] <- rand_RF_2014[i]
      } else {
        IPL_RFith_mc[rownums_for_RF[i]:nrow(IPL_RFith_mc), 1:14] <- rand_RF_2013[i]
        IPL_RFith_mc[rownums_for_RF[i]:nrow(IPL_RFith_mc), 15:18] <- rand_RF_2014[i]
      }
    }

    # calculate IPLareasRF (grams) from peakarea/RF
    IPLareasRF <- IPLareas_mc / IPL_RFith_mc

    # Divide grams by molar mass to calculate moles
    IPLareasRFmol <- sweep(IPLareasRF, MARGIN = 1, masses, '/')


    # create empty vector for next step
    y_frac_ether <- c()
    y_frac_ester <- c()
    y_frac_amide <- c()
    y_frac_nonlinkage <- c()
    y_frac_hydroxylated <- c()
    y_frac_GDGT <- c()
    y_frac_AR <- c()

    # calculate fraction of alkyl chains that are esters, ethers, amides, nonlinked, and hydroxylated
    for (j in 1:ncol(IPLareasRFmol)){
      y_frac_ether <- c(y_frac_ether, sum(IPLareasRFmol[, j]*n_chains_ether_matrix[, j]) / sum(IPLareasRFmol[, j]*n_chains_matrix[, j]))
      y_frac_ester <- c(y_frac_ester, sum(IPLareasRFmol[, j]*n_chains_ester_matrix[, j]) / sum(IPLareasRFmol[, j]*n_chains_matrix[, j]))
      y_frac_amide <- c(y_frac_amide, sum(IPLareasRFmol[, j]*n_chains_amide_matrix[, j]) / sum(IPLareasRFmol[, j]*n_chains_matrix[, j]))
      y_frac_nonlinkage <- c(y_frac_nonlinkage, sum(IPLareasRFmol[, j]*n_chains_nonlinkage_matrix[, j]) / sum(IPLareasRFmol[, j]*n_chains_matrix[, j]))
      y_frac_hydroxylated <- c(y_frac_hydroxylated, sum(IPLareasRFmol[, j]*n_chains_hydroxylated_matrix[, j]) / sum(IPLareasRFmol[, j]*n_chains_matrix[, j]))
      y_frac_GDGT <- c(y_frac_GDGT, sum(IPLareasRFmol[, j]*n_chains_GDGT_matrix[, j]) / sum(IPLareasRFmol[, j]*n_chains_matrix[, j]))
      y_frac_AR <- c(y_frac_AR, sum(IPLareasRFmol[, j]*n_chains_AR_matrix[, j]) / sum(IPLareasRFmol[, j]*n_chains_matrix[, j]))
    }

    # calculate average alkyl chain properties
    IPL_nCave <- colSums(sweep(IPLareasRFmol, MARGIN = 1, nC_raw, "*")) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_nUnsatave <- colSums(sweep(IPLareasRFmol, MARGIN = 1, nUnsat_raw, "*")) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_nPentRingave <- colSums(IPL_nPentRingith * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_nHexRingave <- colSums(IPL_nHexRingith * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_ZCave <- colSums(IPL_ZCith * IPLareasRFmol) / colSums(IPLareasRFmol)

    # replace NaN with 0
    IPL_nCave[is.nan(IPL_nCave)] <- 0
    IPL_nUnsatave[is.nan(IPL_nUnsatave)] <- 0
    IPL_nPentRingave[is.nan(IPL_nPentRingave)] <- 0
    IPL_nHexRingave[is.nan(IPL_nHexRingave)] <- 0
    IPL_ZCave[is.nan(IPL_ZCave)] <- 0


    IPL_C_IPL_ave <- colSums(C_all_IPL_matrix * IPLareasRFmol) / colSums(IPLareasRFmol)
    IPL_C_head_ave <- colSums(C_all_head_matrix * IPLareasRFmol) / colSums(n_heads_matrix * IPLareasRFmol)
    IPL_C_backbone_ave <- colSums(C_all_backbone_matrix * IPLareasRFmol) / colSums(n_backbones_matrix * IPLareasRFmol)
    IPL_C_chain_ave <- colSums(C_all_chain_matrix * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_H_IPL_ave <- colSums(H_all_IPL_matrix * IPLareasRFmol) / colSums(IPLareasRFmol)
    IPL_H_head_ave <- colSums(H_all_head_matrix * IPLareasRFmol) / colSums(n_heads_matrix * IPLareasRFmol)
    IPL_H_backbone_ave <- colSums(H_all_backbone_matrix * IPLareasRFmol) / colSums(n_backbones_matrix * IPLareasRFmol)
    IPL_H_chain_ave <- colSums(H_all_chain_matrix * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_N_IPL_ave <- colSums(N_all_IPL_matrix * IPLareasRFmol) / colSums(IPLareasRFmol)
    IPL_N_head_ave <- colSums(N_all_head_matrix * IPLareasRFmol) / colSums(n_heads_matrix * IPLareasRFmol)
    IPL_N_backbone_ave <- colSums(N_all_backbone_matrix * IPLareasRFmol) / colSums(n_backbones_matrix * IPLareasRFmol)
    IPL_N_chain_ave <- colSums(N_all_chain_matrix * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_O_IPL_ave <- colSums(O_all_IPL_matrix * IPLareasRFmol) / colSums(IPLareasRFmol)
    IPL_O_head_ave <- colSums(O_all_head_matrix * IPLareasRFmol) / colSums(n_heads_matrix * IPLareasRFmol)
    IPL_O_backbone_ave <- colSums(O_all_backbone_matrix * IPLareasRFmol) / colSums(n_backbones_matrix * IPLareasRFmol)
    IPL_O_chain_ave <- colSums(O_all_chain_matrix * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_S_IPL_ave <- colSums(S_all_IPL_matrix * IPLareasRFmol) / colSums(IPLareasRFmol)
    IPL_S_head_ave <- colSums(S_all_head_matrix * IPLareasRFmol) / colSums(n_heads_matrix * IPLareasRFmol)
    IPL_S_backbone_ave <- colSums(S_all_backbone_matrix * IPLareasRFmol) / colSums(n_backbones_matrix * IPLareasRFmol)
    IPL_S_chain_ave <- colSums(S_all_chain_matrix * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_P_IPL_ave <- colSums(P_all_IPL_matrix * IPLareasRFmol) / colSums(IPLareasRFmol)
    IPL_P_head_ave <- colSums(P_all_head_matrix * IPLareasRFmol) / colSums(n_heads_matrix * IPLareasRFmol)
    IPL_P_backbone_ave <- colSums(P_all_backbone_matrix * IPLareasRFmol) / colSums(n_backbones_matrix * IPLareasRFmol)
    IPL_P_chain_ave <- colSums(P_all_chain_matrix * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)
    IPL_Z_IPL_ave <- colSums(Z_all_IPL_matrix * IPLareasRFmol) / colSums(IPLareasRFmol)
    IPL_Z_head_ave <- colSums(Z_all_head_matrix * IPLareasRFmol) / colSums(n_heads_matrix * IPLareasRFmol)
    IPL_Z_backbone_ave <- colSums(Z_all_backbone_matrix * IPLareasRFmol) / colSums(n_backbones_matrix * IPLareasRFmol)
    IPL_Z_chain_ave <- colSums(Z_all_chain_matrix * IPLareasRFmol) / colSums(n_chains_matrix * IPLareasRFmol)


    # For each sample, calculate weighted ZC from weighted elemental abundances
    weighted_IPL_formula <- IPL_C_IPL_ave # initialize vector for chain formulae
    weighted_IPL_ZC <- IPL_C_IPL_ave # initialize vector for weighted ZC
    weighted_head_formula <- IPL_C_head_ave # initialize vector for chain formulae
    weighted_head_ZC <- IPL_C_head_ave # initialize vector for weighted ZC
    weighted_backbone_formula <- IPL_C_backbone_ave # initialize vector for chain formulae
    weighted_backbone_ZC <- IPL_C_backbone_ave # initialize vector for weighted ZC
    weighted_chain_formula <- IPL_C_chain_ave # initialize vector for chain formulae
    weighted_chain_ZC <- IPL_C_chain_ave # initialize vector for weighted ZC


    IPL_ave_formula <- ave_formula(IPL_C_IPL_ave,
                                   IPL_H_IPL_ave,
                                   IPL_O_IPL_ave,
                                   IPL_N_IPL_ave,
                                   IPL_P_IPL_ave,
                                   IPL_S_IPL_ave,
                                   IPL_Z_IPL_ave)
    head_ave_formula <- ave_formula(IPL_C_head_ave,
                                    IPL_H_head_ave,
                                    IPL_O_head_ave,
                                    IPL_N_head_ave,
                                    IPL_P_head_ave,
                                    IPL_S_head_ave,
                                    IPL_Z_head_ave)
    backbone_ave_formula <- ave_formula(IPL_C_backbone_ave,
                                        IPL_H_backbone_ave,
                                        IPL_O_backbone_ave,
                                        IPL_N_backbone_ave,
                                        IPL_P_backbone_ave,
                                        IPL_S_backbone_ave,
                                        IPL_Z_backbone_ave)
    chain_ave_formula <- ave_formula(IPL_C_chain_ave,
                                     IPL_H_chain_ave,
                                     IPL_O_chain_ave,
                                     IPL_N_chain_ave,
                                     IPL_P_chain_ave,
                                     IPL_S_chain_ave,
                                     IPL_Z_chain_ave)

    # For each sample, calculate weighted ZC from weighted elemental abundances
    weighted_IPL_ZC <- lipid_ZC(
      C_cc = signif(IPL_C_IPL_ave, 3),
      H_cc = signif(IPL_H_IPL_ave, 3),
      N_cc = signif(IPL_N_IPL_ave, 3),
      O_cc = signif(IPL_O_IPL_ave, 3),
      P_cc = signif(IPL_P_IPL_ave, 3),
      Zplus_cc = 0,
      Zminus_cc = 0,
      S_cc = signif(IPL_S_IPL_ave, 3),
      Z = signif(IPL_Z_IPL_ave, 3))

    weighted_head_ZC <- lipid_ZC(
      C_cc = signif(IPL_C_head_ave, 3),
      H_cc = signif(IPL_H_head_ave, 3),
      N_cc = signif(IPL_N_head_ave, 3),
      O_cc = signif(IPL_O_head_ave, 3),
      P_cc = signif(IPL_P_head_ave, 3),
      Zplus_cc = 0,
      Zminus_cc = 0,
      S_cc = signif(IPL_S_head_ave, 3),
      Z = signif(IPL_Z_head_ave, 3))

    weighted_backbone_ZC <- lipid_ZC(
      C_cc = signif(IPL_C_backbone_ave, 3),
      H_cc = signif(IPL_H_backbone_ave, 3),
      N_cc = signif(IPL_N_backbone_ave, 3),
      O_cc = signif(IPL_O_backbone_ave, 3),
      P_cc = signif(IPL_P_backbone_ave, 3),
      Zplus_cc = 0,
      Zminus_cc = 0,
      S_cc = signif(IPL_S_backbone_ave, 3),
      Z = signif(IPL_Z_backbone_ave, 3))

    weighted_chain_ZC <- lipid_ZC(
      C_cc = signif(IPL_C_chain_ave, 3),
      H_cc = signif(IPL_H_chain_ave, 3),
      N_cc = signif(IPL_N_chain_ave, 3),
      O_cc = signif(IPL_O_chain_ave, 3),
      P_cc = signif(IPL_P_chain_ave, 3),
      Zplus_cc = 0,
      Zminus_cc = 0,
      S_cc = signif(IPL_S_chain_ave, 3),
      Z = signif(IPL_Z_chain_ave, 3))

    # Create one-dimensional (tall) matrices storing fractions of alkyl chain types.
    # Each should have a length equal to the number of samples where the fraction > 0
    y_frac_ether <- rmv_smpl(y_frac_ether)
    y_frac_ester <- rmv_smpl(y_frac_ester)
    y_frac_amide <- rmv_smpl(y_frac_amide)
    y_frac_nonlinkage <- rmv_smpl(y_frac_nonlinkage)
    y_frac_hydroxylated <- rmv_smpl(y_frac_hydroxylated)
    y_frac_GDGT <- rmv_smpl(y_frac_GDGT)
    y_frac_AR <- rmv_smpl(y_frac_AR)

    weighted_IPL_formula <- rmv_smpl(weighted_IPL_formula)
    weighted_IPL_ZC <- rmv_smpl(weighted_IPL_ZC)
    weighted_head_formula <- rmv_smpl(weighted_head_formula)
    weighted_head_ZC <- rmv_smpl(weighted_head_ZC)
    weighted_backbone_formula <- rmv_smpl(weighted_backbone_formula)
    weighted_backbone_ZC <- rmv_smpl(weighted_backbone_ZC)
    weighted_chain_formula <- rmv_smpl(weighted_chain_formula)
    weighted_chain_ZC <- rmv_smpl(weighted_chain_ZC)

    IPL_C_chain_ave <- rmv_smpl(IPL_C_chain_ave)
    IPL_H_chain_ave <- rmv_smpl(IPL_H_chain_ave)
    IPL_N_chain_ave <- rmv_smpl(IPL_N_chain_ave)
    IPL_O_chain_ave <- rmv_smpl(IPL_O_chain_ave)

    rownames(y_frac_ether) <- colnames(IPLareas)
    rownames(y_frac_ester) <- colnames(IPLareas)
    rownames(y_frac_amide) <- colnames(IPLareas)
    rownames(y_frac_nonlinkage) <- colnames(IPLareas)
    rownames(y_frac_hydroxylated) <- colnames(IPLareas)
    rownames(y_frac_GDGT) <- colnames(IPLareas)
    rownames(y_frac_AR) <- colnames(IPLareas)

    rownames(weighted_IPL_formula) <- colnames(IPLareas)
    rownames(weighted_IPL_ZC) <- colnames(IPLareas)
    rownames(weighted_head_formula) <- colnames(IPLareas)
    rownames(weighted_head_ZC) <- colnames(IPLareas)
    rownames(weighted_backbone_formula) <- colnames(IPLareas)
    rownames(weighted_backbone_ZC) <- colnames(IPLareas)
    rownames(weighted_chain_formula) <- colnames(IPLareas)
    rownames(weighted_chain_ZC) <- colnames(IPLareas)

    rownames(IPL_C_chain_ave) <- colnames(IPLareas)
    rownames(IPL_H_chain_ave) <- colnames(IPLareas)
    rownames(IPL_N_chain_ave) <- colnames(IPLareas)
    rownames(IPL_O_chain_ave) <- colnames(IPLareas)

    # print(weighted_IPL_ZC)

    mipl[["y_frac_ether"]] <- y_frac_ether
    mipl[["y_frac_ester"]] <- y_frac_ester
    mipl[["y_frac_amide"]] <- y_frac_amide
    mipl[["y_frac_nonlinkage"]] <- y_frac_nonlinkage
    mipl[["y_frac_hydroxylated"]] <- y_frac_hydroxylated
    mipl[["y_frac_GDGT"]] <- y_frac_GDGT
    mipl[["y_frac_AR"]] <- y_frac_AR

    mipl[["y_mean_nCith"]] <- IPL_nCave
    mipl[["y_mean_nUnsatith"]] <- IPL_nUnsatave
    mipl[["y_mean_nPentRingith"]] <- IPL_nPentRingave
    mipl[["y_mean_nHexRingith"]] <- IPL_nHexRingave
    mipl[["y_mean_ZCave"]] <- IPL_ZCave

    mipl[["weighted_IPL_formula"]] <- weighted_IPL_formula
    mipl[["weighted_IPL_ZC"]] <- weighted_IPL_ZC
    mipl[["weighted_head_formula"]] <- weighted_head_formula
    mipl[["weighted_head_ZC"]] <- weighted_head_ZC
    mipl[["weighted_backbone_formula"]] <- weighted_backbone_formula
    mipl[["weighted_backbone_ZC"]] <- weighted_backbone_ZC
    mipl[["weighted_chain_formula"]] <- weighted_chain_formula
    mipl[["weighted_chain_ZC"]] <- weighted_chain_ZC

    mipl[["IPL_C_chain_ave"]] <- IPL_C_chain_ave
    mipl[["IPL_H_chain_ave"]] <- IPL_H_chain_ave
    mipl[["IPL_N_chain_ave"]] <- IPL_N_chain_ave
    mipl[["IPL_O_chain_ave"]] <- IPL_O_chain_ave


    IPL_master_monte[[paste(it)]] <- mipl

  } # end M.C. iterations

  options(scipen = default_scipen_setting) # turn scientific notation back on
  print(proc.time()-time)
  return(IPL_master_monte)

}

print("IPL functions loaded...")
