# function to clean and prepare a data frame for plotly/ggplot plotting
cleanup_data <- function(y_to_plot){
  # remove entries with NA or NaN for y
  y_to_plot <- y_to_plot[complete.cases(y_to_plot),]
  # convert to a data frame, which can undergo binning
  y_to_plot <- as.data.frame(y_to_plot)
  # bin data for boxplots
  y_to_plot$bin <- cut(y_to_plot$x, c(-Inf, seq(min(y_to_plot$x), max(y_to_plot$x), length.out=75), Inf))
  # remove entries with NA or NaN for bins
  y_to_plot <- y_to_plot[complete.cases(y_to_plot),]
  return(y_to_plot)
}

create_lineplot_pe <- function(spring, min, max, by){
  t <- readRDS(paste0("rds/thermo_results_", spring, ".rds"))

  number_of_ys <- ncol(t)-1

  sample_prefix <- "L"

  for(i in 1:number_of_ys){
    var_name <- paste("y", i, sep="")
    assign(var_name, t[var_name])
  }
  x  <- t$x[seq(1, nrow(y1))] # Eh


  pal <- c("#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#004D40", "#1E88E5", "#FFC107", "#D81B60", "#CC73C6", "#B55276", "#648FFF", "#785EF0", "#DC267F", "#FE6100", "#FFB000", "#105C4D")
  pal_2 <- c("black")

  # palette list to evaluate in ggplot scale_fill_manual(values = xxx)
  pal_list_to_eval <- "c("
  pal_list_to_eval_2 <- "c("
  for(i in 1:number_of_ys-1){
    pal_list_to_eval <- paste(pal_list_to_eval, sample_prefix, i, " = pal[", i, "], ", sep="")
    pal_list_to_eval_2 <- paste(pal_list_to_eval_2, sample_prefix, i, " = pal_2[", i, "], ", sep="")
  }
  pal_list_to_eval <- paste(pal_list_to_eval, sample_prefix, number_of_ys, " = pal[", number_of_ys, "])", sep="")
  pal_list_to_eval_2 <- paste(pal_list_to_eval_2, sample_prefix, number_of_ys, " = pal_2[", number_of_ys, "])", sep="")

  # bind x and y of interest
  for(i in 1:number_of_ys){
    var_name <- paste("y", i, "_to_plot", sep="")
    assign(var_name, cbind(x = t$x, y = as.vector(unlist(t[paste("y", i, sep="")]))))
    assign(var_name, cleanup_data(get(var_name))) # cleans out incomplete x-y cases
    assign(var_name, cbind(get(var_name), site = paste(sample_prefix, i, sep="")))
  }

  y_to_plot <- y1_to_plot
  for(i in 2:number_of_ys){
    var_name <- paste("y", i, "_to_plot", sep="")
    y_to_plot <- rbind(y_to_plot, get(var_name))
  }
  y_to_plot <- as.data.frame(y_to_plot)

  # plot boxplot in ggplot and plotly
  library(plyr)
  library(reshape2)
  library(ggplot2)
  p <- ggplot(t, aes(x = x, y = y1)) +
         theme(panel.background = element_blank()) +
         # labs(x = "Eh, volts", y = "% abundance")
         labs(x = "pe", y = "degree of formation (%)")

  for(i in 1:number_of_ys){
    var_name = paste("y", i, sep="")
    site_name <- paste(sample_prefix, i, sep="")
    p <- p + geom_line(data=t, aes_string(x="x", y=var_name, colour = shQuote(site_name)), size=1, inherit.aes=FALSE)
  }

  # add vertical line at lipid-predicted Eh (max abundance) for the spring
    if(spring == "Bison OF1"){
      i <- 1
    } else if(spring == "Bison OF2"){
      i <- 2
    } else if(spring == "Bison OF3"){
      i <- 3
    } else if(spring == "Bison OF4"){
      i <- 4
    } else if(spring == "Bison OF5"){
      i <- 5
    } else if(spring == "Bison OF6"){
      i <- 6
    } else if(spring == "Mound OF1"){
      i <- 7
    } else if(spring == "Mound OF2"){
      i <- 8
    } else if(spring == "Mound OF3"){
      i <- 9
    } else if(spring == "Mound OF4"){
      i <- 10
    } else if(spring == "Mound OF5"){
      i <- 11
    } else if(spring == "Empress OF1"){
      i <- 12
    } else if(spring == "Empress OF2"){
      i <- 13
    } else if(spring == "Empress OF3"){
      i <- 14
    } else if(spring == "Empress OF4"){
      i <- 15
    } else if(spring == "Empress OF5"){
      i <- 16
    } else if(spring == "Octopus OF1"){
      i <- 17
    } else if(spring == "Octopus OF2"){
      i <- 18
    }

  this_var <- paste0("y", i) # the variable of this spring, e.g. "y2" when calculated for conditions of Bison Sample2

  if(spring == "Octopus OF1" | spring == "Bison OF6"){
    maxabund <- 95 # 95% height cutoff for first and last outflow sites
  } else {
    maxabund <- max(t[, this_var]) # maximum abundance of this site (e.g 89)
  }
  Eh_predict <- t[which.min(abs(t[, this_var] - maxabund)), "x"] # find Eh closest to max abundance or 95% height cutoff
  print(paste(spring, Eh_predict))
  # p <- p + geom_segment(aes(x = Eh_predict, y = 0, xend = Eh_predict, yend = maxabund), linetype = 2, colour = pal[as.numeric(i)]) # vline ends at max abund
  p <- p + geom_vline(xintercept = Eh_predict, linetype = 2, size = 1, colour = pal[as.numeric(i)]) # vline spans entire plot


  p <- p + scale_colour_manual(name = sample_prefix, values = eval(parse(text = pal_list_to_eval)))
  p <- p + scale_x_continuous(breaks = seq(min, max, by))
  p <- p + theme(
    axis.text.y = element_text(colour="black"),
    axis.text.x = element_text(colour="black", angle = 0),
    panel.background = element_blank(),
    # legend.position = "none", # remove legend completely
    legend.title = element_blank(), # remove legend title
    legend.key=element_blank() # remove gray background behind legend keys

  )

  dir.create(file.path(getwd(), "plots"), showWarnings = FALSE)

  # png(file=paste0("plots//", spring, "_thermo.png"), width=10, height=4, units = 'in', res = 600) # create png
  pdf(file=paste0("plots//", spring, "_thermo_pe.pdf"), width = 10, height = 4) # create pdf
  print(p)# + theme(legend.position="none"))
  dev.off()

  return(Eh_predict)

}
