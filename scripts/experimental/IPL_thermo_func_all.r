# funtion to solve for y using x and a 'lm'-linear model
lm_solve <- function (x, lm_object){
  yint <- lm_object[1]$coefficients[1]
  slope <- lm_object[1]$coefficients[2]
  y <- x * slope + yint
  return(y)
}

closest <- function(xv, sv){
  xv[which(abs(xv - sv) == min(abs(xv - sv)))]
}

# Function to estimate thermodynamic properties of 'average lipid chains'
# y_frac values are the mole fraction of observed IPL alkyl chains at a
#   sample site with a given chain-backbone linkage type.
# y_mean values are the average number of a given modification per alkyl chain.
# _prop values are thermodynamic properties (V, Cp, Gf, etc.) associated with
#   a given linkage type or chain modification.
ave_prop_solve <- function(ave_chain_C, y_frac_ester, y_frac_ether,
  y_frac_amide, y_frac_nonlinkage, y_mean_nUnsatith, y_mean_nPentRingith,
  y_mean_nHexRingith, fatty_acid_prop, fatty_alcohol_prop, alkane_prop,
  carb_acid_to_amide_prop, delta_unsat_prop, y_frac_hydroxylated,
  y_frac_GDGT, y_frac_AR, delta_hydroxyl_prop, delta_GDGT_prop,
  delta_branch_prop, delta_pent_ring_prop, delta_hex_ring_prop){

  ave_chain_property <- ( # begin average chain property calculation
    # ester
    y_frac_ester * lm_solve(ave_chain_C, fatty_acid_prop) +

    # ether
    y_frac_ether * lm_solve(ave_chain_C, fatty_alcohol_prop) +

    # C-C
    y_frac_nonlinkage * lm_solve(ave_chain_C, alkane_prop) +

    # amide
    (y_frac_amide * (lm_solve(ave_chain_C, fatty_acid_prop) + carb_acid_to_amide_prop)) +

    # unsaturation
    (y_mean_nUnsatith * delta_unsat_prop) +

    # hydroxylations
    (y_frac_hydroxylated * delta_hydroxyl_prop) +

    # GDGTs
    (y_frac_GDGT * lm_solve(ave_chain_C, fatty_alcohol_prop) + delta_GDGT_prop) +

    # isoprenoidal branching
    # Add difference between straight and branching chain. One branch per 5 carbons.
    # One internal ring will replace one branch, so subract ring contribution.
    ((y_frac_GDGT + y_frac_AR) * (ave_chain_C/5 - y_mean_nPentRingith - y_mean_nHexRingith) * delta_branch_prop) +

    # internal pentacyclic and hexacyclic rings
    (y_mean_nPentRingith * delta_pent_ring_prop) +
    (y_mean_nHexRingith * delta_hex_ring_prop)

  ) # end ave_chain_property calculation

  return(ave_chain_property)

}

IPL_thermo <- function(IPL_master, this_IPL, IPLthermo_directory,
  iterations = 1, my_basis = NULL, my_activities = NULL, my_species = NULL,
  my_T = 25.0, my_balance = 1, res = 150, x_rng = c(-0.60, -0.25),
  save_object = TRUE, thermo_method = "affinity()", my_sample = NULL,
  calc_Eh = FALSE, create_pub_table = FALSE, save_calc_Eh_to_rds = FALSE, calc_Eh_rds_name = NULL){

  # create empty vectors to store results
  x <- c()
  for(i in 1:length(my_species)){
    assign(paste("y", i, sep=""), as.vector(NULL))
  }

  i <- 1

  # unpack stuff from IPL_master.
  mipl <- IPL_master[[this_IPL]] #this_IPL can be something like "1G-DAG" or "mergedIPL 1"

  # calculate fraction of IPLs that are esters, ethers, amides, nonlinked, and hydroxylated
  y_frac_ether <- mipl[["y_frac_ether"]]
  y_frac_ester <- mipl[["y_frac_ester"]]
  y_frac_amide <- mipl[["y_frac_amide"]]
  y_frac_nonlinkage <- mipl[["y_frac_nonlinkage"]]
  y_frac_hydroxylated <- mipl[["y_frac_hydroxylated"]]
  y_frac_GDGT <- mipl[["y_frac_GDGT"]]
  y_frac_AR <- mipl[["y_frac_AR"]]
  y_mean_nCith <- mipl[["y_mean_nCith"]]
  y_mean_nUnsatith <- mipl[["y_mean_nUnsatith"]]
  y_mean_nPentRingith <- mipl[["y_mean_nPentRingith"]]
  y_mean_nHexRingith <- mipl[["y_mean_nHexRingith"]]

  y_frac_ether_vec <- c()
  y_frac_ester_vec <- c()
  y_frac_amide_vec <- c()
  y_frac_nonlinkage_vec <- c()
  y_frac_GDGT_vec <- c()
  y_frac_AR_vec <- c()
  y_frac_ether_vec <- c()
  y_frac_ether_vec <- c()
  y_mean_nCith_vec <- c()
  y_mean_nUnsatith_vec <- c()
  y_mean_nPentRingith_vec <- c()
  y_mean_nHexRingith_vec <- c()
  y_frac_hydroxylated_vec <- c()

  ### using method in Ch2 of thesis to get chain elemental abundances
  # load average chain elemental composition from IPL_master
  IPL_C_chain_ave <- mipl[["IPL_C_chain_ave"]]
  IPL_H_chain_ave <- mipl[["IPL_H_chain_ave"]]
  IPL_N_chain_ave <- mipl[["IPL_N_chain_ave"]]
  IPL_O_chain_ave <- mipl[["IPL_O_chain_ave"]]

  C_linked <- IPL_C_chain_ave
  H_linked <- IPL_H_chain_ave
  N_linked <- IPL_N_chain_ave
  O_linked <- IPL_O_chain_ave
  x_cc <- y_frac_nonlinkage
  x_amide <- y_frac_amide
  x_ester <- y_frac_ester
  x_ether <- y_frac_ether + y_frac_GDGT

  C_free <- C_linked + x_cc
  H_free <- H_linked + x_ether + x_ester + 2*x_amide + 3*x_cc
  N_free <- N_linked + x_amide
  O_free <- O_linked + x_ether + x_ester

  C_free <- signif(C_free, 3)
  H_free <- signif(H_free, 3)
  N_free <- signif(N_free, 3)
  O_free <- signif(O_free, 3)

  ave_chain_C <- C_free
  ave_chain_formula <- lipid_formula(C_free, H_free, N_free, O_free, 0, 0, 0, 0)

  ### calculate average chain thermo properties Gf, Gh, Hf, S, Cp, V
  # import thermo properties workbook
  worksheet <- suppressMessages(as.data.frame(read_excel(paste0(IPLthermo_directory, "chain_thermo.xlsx"), sheet = "Main")))

  # chain length vs. all properties
  wkst_lookup <- function(row_lookup, col_lookup, type = "name"){
    return(as.numeric(worksheet[which(worksheet[, type] == row_lookup), col_lookup]))
  }
  nC_fatty_acid <- wkst_lookup("carb_acid", "nC", type = "type")
  Gh_fatty_acid <- wkst_lookup("carb_acid", "G of hydration", type = "type")
  Gf_fatty_acid <- wkst_lookup("carb_acid", "G of formation", type = "type")
  Hf_fatty_acid <- wkst_lookup("carb_acid", "H of formation", type = "type")
  V_fatty_acid <- wkst_lookup("carb_acid", "V (aq)", type = "type")
  Cp_fatty_acid <- wkst_lookup("carb_acid", "Cp (aq)", type = "type")
  nC_fatty_alcohol <- wkst_lookup("primary_alcohol", "nC", type = "type")
  Gh_fatty_alcohol <- wkst_lookup("primary_alcohol", "G of hydration", type = "type")
  Gf_fatty_alcohol <- wkst_lookup("primary_alcohol", "G of formation", type = "type")
  Hf_fatty_alcohol <- wkst_lookup("primary_alcohol", "H of formation", type = "type")
  V_fatty_alcohol <- wkst_lookup("primary_alcohol", "V (aq)", type = "type")
  Cp_fatty_alcohol <- wkst_lookup("primary_alcohol", "Cp (aq)", type = "type")
  nC_fatty_alkane <- wkst_lookup("alkane", "nC", type = "type")
  Gh_fatty_alkane <- wkst_lookup("alkane", "G of hydration", type = "type")
  Gf_fatty_alkane <- wkst_lookup("alkane", "G of formation", type = "type")
  Hf_fatty_alkane <- wkst_lookup("alkane", "H of formation", type = "type")
  V_fatty_alkane <- wkst_lookup("alkane", "V (aq)", type = "type")
  Cp_fatty_alkane <- wkst_lookup("alkane", "Cp (aq)", type = "type")

  # estimated difference between a carb. acid and an amide
  Gh_carb_acid_to_amide <- wkst_lookup("carb acid to amide", "G of hydration")
  Gf_carb_acid_to_amide <- wkst_lookup("carb acid to amide", "G of formation")
  Hf_carb_acid_to_amide <- wkst_lookup("carb acid to amide", "H of formation")
  V_carb_acid_to_amide <- wkst_lookup("carb acid to amide", "V (aq)")
  Cp_carb_acid_to_amide <- wkst_lookup("carb acid to amide", "Cp (aq)")

  # estimated difference between a saturated and monounsaturated alkyl chain
  Gh_delta_unsat <- wkst_lookup("delta unsat", "G of hydration")
  Gf_delta_unsat <- wkst_lookup("delta unsat", "G of formation")
  Hf_delta_unsat <- wkst_lookup("delta unsat", "H of formation")
  V_delta_unsat <- wkst_lookup("delta unsat", "V (aq)")
  Cp_delta_unsat <- wkst_lookup("delta unsat", "Cp (aq)")

  # estimated difference between an alkane and a secondary hydroxyl
  Gh_delta_hydroxyl <- wkst_lookup("alkane to secondary hydroxyl", "G of hydration")
  Gf_delta_hydroxyl <- wkst_lookup("alkane to secondary hydroxyl", "G of formation")
  Hf_delta_hydroxyl <- wkst_lookup("alkane to secondary hydroxyl", "H of formation")
  V_delta_hydroxyl <- wkst_lookup("alkane to secondary hydroxyl", "V (aq)")
  Cp_delta_hydroxyl <- wkst_lookup("alkane to secondary hydroxyl", "Cp (aq)")

  # estimated difference between a fatty alcohol and a fatty alcohol with a terminal CH2 instead of a CH3
  Gh_delta_GDGT <- wkst_lookup("fatty alcohol CH3 to CH2", "G of hydration")
  Gf_delta_GDGT <- wkst_lookup("fatty alcohol CH3 to CH2", "G of formation")
  Hf_delta_GDGT <- wkst_lookup("fatty alcohol CH3 to CH2", "H of formation")
  V_delta_GDGT <- wkst_lookup("fatty alcohol CH3 to CH2", "V (aq)")
  Cp_delta_GDGT <- wkst_lookup("fatty alcohol CH3 to CH2", "Cp (aq)")

  # estimated difference between a straight chain and branching chain
  Gh_delta_branch <- wkst_lookup("delta branch", "G of hydration")
  Gf_delta_branch <- wkst_lookup("delta branch", "G of formation")
  Hf_delta_branch <- wkst_lookup("delta branch", "H of formation")
  V_delta_branch <- wkst_lookup("delta branch", "V (aq)")
  Cp_delta_branch <- wkst_lookup("delta branch", "Cp (aq)")

  # estimated difference between a straight chain and chain with internal pentacyclic ring
  Gh_delta_pent_ring <- wkst_lookup("delta cyclopentane ring", "G of hydration")
  Gf_delta_pent_ring <- wkst_lookup("delta cyclopentane ring", "G of formation")
  Hf_delta_pent_ring <- wkst_lookup("delta cyclopentane ring", "H of formation")
  V_delta_pent_ring <- wkst_lookup("delta cyclopentane ring", "V (aq)")
  Cp_delta_pent_ring <- wkst_lookup("delta cyclopentane ring", "Cp (aq)")

  # estimated difference between a straight chain and chain with internal hexacyclic ring
  Gh_delta_hex_ring <- wkst_lookup("delta cyclohexane ring", "G of hydration")
  Gf_delta_hex_ring <- wkst_lookup("delta cyclohexane ring", "G of formation")
  Hf_delta_hex_ring <- wkst_lookup("delta cyclohexane ring", "H of formation")
  V_delta_hex_ring <- wkst_lookup("delta cyclohexane ring", "V (aq)")
  Cp_delta_hex_ring <- wkst_lookup("delta cyclohexane ring", "Cp (aq)")

  ### fit available thermo data vs. nC (>=3 carbons) with a linear model
  lm_nC <- function(this_prop, this_nC, nC_minimum){
    return(lm(this_prop[this_nC >= nC_minimum] ~
      this_nC[this_nC >= nC_minimum], na.action = na.exclude))
  }

  nC_min <- 3

  Gh_vs_nC_fatty_acid <- lm_nC(Gh_fatty_acid, nC_fatty_acid, nC_min)
  Gf_vs_nC_fatty_acid <- lm_nC(Gf_fatty_acid, nC_fatty_acid, nC_min)
  Hf_vs_nC_fatty_acid <- lm_nC(Hf_fatty_acid, nC_fatty_acid, nC_min)
  V_vs_nC_fatty_acid <- lm_nC(V_fatty_acid, nC_fatty_acid, nC_min)
  Cp_vs_nC_fatty_acid <- lm_nC(Cp_fatty_acid, nC_fatty_acid, nC_min)

  Gh_vs_nC_fatty_alcohol <- lm_nC(Gh_fatty_alcohol, nC_fatty_alcohol, nC_min)
  Gf_vs_nC_fatty_alcohol <- lm_nC(Gf_fatty_alcohol, nC_fatty_alcohol, nC_min)
  Hf_vs_nC_fatty_alcohol <- lm_nC(Hf_fatty_alcohol, nC_fatty_alcohol, nC_min)
  V_vs_nC_fatty_alcohol <- lm_nC(V_fatty_alcohol, nC_fatty_alcohol, nC_min)
  Cp_vs_nC_fatty_alcohol <- lm_nC(Cp_fatty_alcohol, nC_fatty_alcohol, nC_min)

  Gh_vs_nC_fatty_alkane <- lm_nC(Gh_fatty_alkane, nC_fatty_alkane, nC_min)
  Gf_vs_nC_fatty_alkane <- lm_nC(Gf_fatty_alkane, nC_fatty_alkane, nC_min)
  Hf_vs_nC_fatty_alkane <- lm_nC(Hf_fatty_alkane, nC_fatty_alkane, nC_min)
  V_vs_nC_fatty_alkane <- lm_nC(V_fatty_alkane, nC_fatty_alkane, nC_min)
  Cp_vs_nC_fatty_alkane <- lm_nC(Cp_fatty_alkane, nC_fatty_alkane, nC_min)

  # solve for properties of 'average chains' by blending their linkage types
  ave_chain_Gh <- ave_prop_solve(ave_chain_C, y_frac_ester, y_frac_ether,
    y_frac_amide, y_frac_nonlinkage, y_mean_nUnsatith, y_mean_nPentRingith,
    y_mean_nHexRingith, Gh_vs_nC_fatty_acid, Gh_vs_nC_fatty_alcohol,
    Gh_vs_nC_fatty_alkane, Gh_carb_acid_to_amide, Gh_delta_unsat,
    y_frac_hydroxylated, y_frac_GDGT, y_frac_AR, Gh_delta_hydroxyl,
    Gh_delta_GDGT, Gh_delta_branch, Gh_delta_pent_ring, Gh_delta_hex_ring)
  ave_chain_Gf <- ave_prop_solve(ave_chain_C, y_frac_ester, y_frac_ether,
    y_frac_amide, y_frac_nonlinkage, y_mean_nUnsatith, y_mean_nPentRingith,
    y_mean_nHexRingith, Gf_vs_nC_fatty_acid, Gf_vs_nC_fatty_alcohol,
    Gf_vs_nC_fatty_alkane, Gf_carb_acid_to_amide, Gf_delta_unsat,
    y_frac_hydroxylated, y_frac_GDGT, y_frac_AR, Gf_delta_hydroxyl,
    Gf_delta_GDGT, Gf_delta_branch, Gf_delta_pent_ring, Gf_delta_hex_ring)
  ave_chain_Hf <- ave_prop_solve(ave_chain_C, y_frac_ester, y_frac_ether,
    y_frac_amide, y_frac_nonlinkage, y_mean_nUnsatith, y_mean_nPentRingith,
    y_mean_nHexRingith, Hf_vs_nC_fatty_acid, Hf_vs_nC_fatty_alcohol,
    Hf_vs_nC_fatty_alkane, Hf_carb_acid_to_amide, Hf_delta_unsat,
    y_frac_hydroxylated, y_frac_GDGT, y_frac_AR, Hf_delta_hydroxyl,
    Hf_delta_GDGT, Hf_delta_branch, Hf_delta_pent_ring, Hf_delta_hex_ring)
  ave_chain_V <- ave_prop_solve(ave_chain_C, y_frac_ester, y_frac_ether,
    y_frac_amide, y_frac_nonlinkage, y_mean_nUnsatith, y_mean_nPentRingith,
    y_mean_nHexRingith, V_vs_nC_fatty_acid, V_vs_nC_fatty_alcohol,
    V_vs_nC_fatty_alkane, V_carb_acid_to_amide, V_delta_unsat,
    y_frac_hydroxylated, y_frac_GDGT, y_frac_AR, V_delta_hydroxyl,
    V_delta_GDGT, V_delta_branch, V_delta_pent_ring, V_delta_hex_ring)
  ave_chain_Cp <- ave_prop_solve(ave_chain_C, y_frac_ester, y_frac_ether,
    y_frac_amide, y_frac_nonlinkage, y_mean_nUnsatith, y_mean_nPentRingith,
    y_mean_nHexRingith, Cp_vs_nC_fatty_acid, Cp_vs_nC_fatty_alcohol,
    Cp_vs_nC_fatty_alkane, Cp_carb_acid_to_amide, Cp_delta_unsat,
    y_frac_hydroxylated, y_frac_GDGT, y_frac_AR, Cp_delta_hydroxyl,
    Cp_delta_GDGT, Cp_delta_branch, Cp_delta_pent_ring, Cp_delta_hex_ring)

  suppressMessages(library(CHNOSZ))
  suppressMessages(reset())

  # solve for S(aq) using Gf, Hf, and Selements
  # ave_chain_Gf = ave_chain_Hf - 298.15*(ave_chain_S - ave_chain_Selements)
  # print(ave_chain_formula)
  ave_chain_Selements <- entropy(ave_chain_formula)*4.184

  ave_chain_S <- (((ave_chain_Gf*1000) - (ave_chain_Hf*1000)) / (-298.15)) + ave_chain_Selements

  # The charge of an average IPL chain is assumed to be 0 here.
  # (ave_chain_charge needs the same number of elements as other ave_chain
  # properties, so just use Gf and set to 0)
  ave_chain_charge <- ave_chain_Gf * 0


  # solve for HKF params of average chains
  suppressWarnings({ # function works but still needs to be properly vectorized to avoid warnings
    HKFparams <- findHKF(Gh = ave_chain_Gh, Vh = ave_chain_V,
      Cp = ave_chain_Cp, Gf = ave_chain_Gf, Hf = ave_chain_Hf,
      Saq = ave_chain_S, charge = ave_chain_charge)
  })


  # create a table that can be added to OBIGT (in CHNOSZ)
  IPL_OBIGT <- data.frame(
    name = rownames(HKFparams$G),
    abbrv = rep("NA", length(HKFparams$G)),
    formula = ave_chain_formula,
    state = rep("aq", length(HKFparams$G)),
    ref1 =  rep("thermofunc", length(HKFparams$G)),
    ref2 =  rep("NA", length(HKFparams$G)),
    date = rep(Sys.Date(), length(HKFparams$G)),
    G = HKFparams$G,
    H = HKFparams$H,
    S = HKFparams$S,
    Cp = HKFparams$Cp,
    V = HKFparams$V,
    a1.a = HKFparams$a1,
    a2.b = HKFparams$a2,
    a3.c = HKFparams$a3,
    a4.d = HKFparams$a4,
    c1.e = HKFparams$c1,
    c2.f = HKFparams$c2,
    omega.lambda = HKFparams$omega,
    z.T = HKFparams$Z)

  if(create_pub_table){

    # format chain formulae for LaTeX
    ave_chain_formula_LaTeX <- c()
    for(i in 1:length(ave_chain_formula)){
      C_cc <- makeup(ave_chain_formula[i])["C"]
      H_cc <- makeup(ave_chain_formula[i])["H"]
      N_cc <- makeup(ave_chain_formula[i])["N"]
      O_cc <- makeup(ave_chain_formula[i])["O"]
      ave_chain_formula_LaTeX <- c(ave_chain_formula_LaTeX, lipid_formula_LaTeX(C_cc, H_cc, N_cc, O_cc, 0, 0, 0, 0))
    }

    # create an OBIGT-style table using joule-based units for published tables, etc.
    IPL_OBIGT_joule <- data.frame(
      name = rownames(HKFparams$G),
      abbrv = rep("NA", length(HKFparams$G)),
      formula = ave_chain_formula_LaTeX,
      state = rep("aq", length(HKFparams$G)),
      ref1 =  rep("thermofunc", length(HKFparams$G)),
      ref2 =  rep("NA", length(HKFparams$G)),
      date = rep(Sys.Date(), length(HKFparams$G)),
      G = HKFparams$G * (4.184/1000), # kJ/mol
      H = HKFparams$H * (4.184/1000), # kJ/mol
      S = HKFparams$S * 4.184, # J/mol K
      Cp = HKFparams$Cp * 4.184, # J/mol K
      V = HKFparams$V, # cm3
      a1.a = HKFparams$a1 * 4.184,
      a2.b = HKFparams$a2 * 4.184,
      a3.c = HKFparams$a3 * 4.184,
      a4.d = HKFparams$a4 * 4.184,
      c1.e = HKFparams$c1 * 4.184,
      c2.f = HKFparams$c2 * 4.184,
      omega.lambda = HKFparams$omega * 4.184,
      Gh = ave_chain_Gh)

  }

  # modify OBIGT data frame to use an appropriate number of sigfigs

  # load csv containing # of significant figures used to report OBIGT values
  df_sigfig <- as.data.frame(read.csv(paste0(IPLthermo_directory, "IPL_OBIGT_sigfig.csv")))
  # convert BHP_OBIGT to the correct number of significant figures

  for(col in colnames(df_sigfig)){
    if (col %in% colnames(IPL_OBIGT)){
      IPL_OBIGT[, col] <- signif(IPL_OBIGT[, col], df_sigfig[, col])
      if(create_pub_table){
        IPL_OBIGT_joule[, col] <- signif(IPL_OBIGT_joule[, col], df_sigfig[, col])
      }
    }
  }

  write.table(IPL_OBIGT, file = paste0(IPLthermo_directory, "IPL_OBIGT.csv"), append = FALSE, sep = ",", row.names = FALSE)

  if(create_pub_table){
    # sort rows in order of pub
    table_order <- c("Bison OF1", "Bison OF2", "Bison OF3", "Bison OF4", "Bison OF5", "Bison OF6",
                      "Mound OF1", "Mound OF2", "Mound OF3", "Mound OF4", "Mound OF5",
                      "Empress OF1", "Empress OF2", "Empress OF3", "Empress OF4", "Empress OF5",
                      "Octopus OF1", "Octopus OF2")
    IPL_OBIGT_joule <- IPL_OBIGT_joule[table_order, ]
    write.table(IPL_OBIGT_joule, file = paste0(IPLthermo_directory, "IPL_OBIGT_for_pub.csv"), append = FALSE, sep = ",", row.names = FALSE)
  }


  mySpecies <- suppressMessages(add.obigt(paste0(IPLthermo_directory, "IPL_OBIGT.csv"))) # a chnosz function

  suppressMessages(basis(my_basis, my_activities))
  species(mySpecies[my_species])

  # perform thermodynamic calculation using affinity() and equilibrate()
  if(thermo_method == "affinity()"){
    suppressMessages(basis(my_basis, my_activities))
    suppressMessages(species(mySpecies[my_species]))
    a <- suppressMessages(affinity(Eh = c(x_rng[1], x_rng[2], res), T = my_T))
    e <- suppressMessages(equilibrate(a, balance = my_balance))
  } else if (thermo_method == "mosaic()"){
    if("NH3" %in% my_basis || "NH4+" %in% my_basis){
      m <- suppressMessages(mosaic(c("NH3","NH4+"), c("HCO3-","CO3-2","CO2"), Eh = c(x_rng[1], x_rng[2], res), T = my_T))
    } else if("NO3-" %in% my_basis || "NO2-" %in% my_basis){
      m <- suppressMessages(mosaic(c("HCO3-","CO3-2","CO2"), Eh = c(x_rng[1], x_rng[2], res), T = my_T))
    }
    e <- suppressMessages(equilibrate(m$A.species, balance = my_balance))
  } else if (thermo_method == "mosaic() pe"){
    if("NH3" %in% my_basis || "NH4+" %in% my_basis){
      m <- suppressMessages(mosaic(c("NH3","NH4+"), c("HCO3-","CO3-2","CO2"), pe = c(x_rng[1], x_rng[2], res), T = my_T))
    } else if("NO3-" %in% my_basis || "NO2-" %in% my_basis){
      m <- suppressMessages(mosaic(c("HCO3-","CO3-2","CO2"), pe = c(x_rng[1], x_rng[2], res), T = my_T))
    }
    e <- suppressMessages(equilibrate(m$A.species, balance = my_balance))
  } else {
    stop("Error: Thermo calculation method not recognized. Try 'affinity()' or 'mosaic()'.")
  }


  # prepare 'average lipid' speciation output for plotting:
  # populates Eh or pe
  x  <- c(x, round(seq(from = x_rng[1], to = x_rng[2], length.out = res), 5))

  # populates y1 though y# with percent speciation,
  # where # is the number of samples
  for(ii in 1:length(my_species)){
    var_name <- paste("y", ii, sep="")
    assign(var_name, append(get(var_name),
      round(10^e$loga.equil[[ii]]/10^e$loga.balance, 5)*100))
  }



  thermo_out <- data.frame(cbind(x, y1))
  for(i in 2:length(my_species)){
    var_name <- paste("y", i, sep="")
    thermo_out <- cbind(thermo_out, get(var_name))
    names(thermo_out)[i+1] <- var_name
  }
  thermo_out <- as.data.frame(thermo_out)

  # calculate IPL-predicted Eh
  if (calc_Eh){

    is.nan.data.frame <- function(x)
    do.call(cbind, lapply(x, is.nan))
    thermo_out[is.nan(thermo_out)] <- 0

    


    if(spring == "Bison OF1"){
      i <- 1
    } else if(spring == "Bison OF2"){
      i <- 2
    } else if(spring == "Bison OF3"){
      i <- 3
    } else if(spring == "Bison OF4"){
      i <- 4
    } else if(spring == "Bison OF5"){
      i <- 5
    } else if(spring == "Bison OF6"){
      i <- 6
    } else if(spring == "Mound OF1"){
      i <- 7
    } else if(spring == "Mound OF2"){
      i <- 8
    } else if(spring == "Mound OF3"){
      i <- 9
    } else if(spring == "Mound OF4"){
      i <- 10
    } else if(spring == "Mound OF5"){
      i <- 11
    } else if(spring == "Empress OF1"){
      i <- 12
    } else if(spring == "Empress OF2"){
      i <- 13
    } else if(spring == "Empress OF3"){
      i <- 14
    } else if(spring == "Empress OF4"){
      i <- 15
    } else if(spring == "Empress OF5"){
      i <- 16
    } else if(spring == "Octopus OF1"){
      i <- 17
    } else if(spring == "Octopus OF2"){
      i <- 18
    }
    var_name <- paste0("y", i)

    max_abund <- max(thermo_out[, var_name], na.rm=TRUE)
    if (my_sample != "Octopus OF1" & my_sample != "Bison OF6"){
      predicted_Eh <- thermo_out[which(thermo_out[, var_name] == max_abund), "x"]
      my_x <- max_abund
    } else {
      closest_to_95 <- closest(thermo_out[, var_name], max_abund*0.95)[1]
      predicted_Eh <- thermo_out[which(thermo_out[, var_name] == closest_to_95), "x"]
      my_x <- closest_to_95
    }
    if (length(predicted_Eh) > 1){ # pre-empt scenario where there is more than one result
      predicted_Eh <- predicted_Eh[1]
      max_abund <- thermo_out[which(thermo_out[, "x"] == predicted_Eh), var_name]
    }
    #print(paste(my_sample, predicted_Eh))
    if(save_calc_Eh_to_rds){
      calc_Eh_list <- readRDS(calc_Eh_rds_name)
      calc_Eh_list[[my_sample]] <- c(calc_Eh_list[[my_sample]], predicted_Eh)
      saveRDS(calc_Eh_list, calc_Eh_rds_name)
    }
  }

  if(save_object){
    # save thermo results
    saveRDS(thermo_out, paste0("rds/thermo_results_", my_sample, ".rds"))
  }
  
  return(thermo_out)
}

print("Thermo functions loaded")
