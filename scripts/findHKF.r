findHKF <- function(Gh = NA, Vh = NA, Cp = NA, Gf = NA, Hf = NA, Saq = NA, charge = NA){

  # define eta (angstroms*cal/mol)
  eta <- (1.66027*10^5)

  # define YBorn (1/K)
  YBorn <- -5.81*10^-5

  # define QBorn (1/bar)
  QBorn <- 5.90*10^-7

  # define XBorn (1/K^2)
  XBorn <- -3.09*10^-7

  # define abs_protonBorn (cal/mol), mentioned in text after Eq 47 in Shock and Helgeson 1988
  abs_protonBorn <- (0.5387 * 10^5)


  if (!is.na(Gh) & charge == 0){

    # find omega*10^-5 (j/mol) if neutral and Gh available
    HKFomega <- 2.61+(324.1/(Gh-90.6)) # Eq 8 in Plyasunov and Shock 2001

  } else if (charge == 0) {

    # find omega*10^-5 (j/mol) if neutral and Gh unavailable
    HKFomega <- (10^-5)*((-1514.4*(Saq/4.184)) + (0.34*10^5))*4.184 # Eq 61 in Shock and Helgeson 1990 for NONVOLATILE neutral organic species

  } else if (charge != 0) {

    # define alphaZ (described in text after Eq 59 in Shock and Helgeson 1990)
    if (abs(charge) == 1){
      alphaZ <- 72
    } else if (abs(charge) == 2){
      alphaZ <- 141
    } else if (abs(charge) == 3){
      alphaZ <- 211
    } else if (abs(charge) == 4){
      alphaZ <- 286
    } else {
      alphaZ <- NA
    }

    # define BZ
    BZ <- ((-alphaZ*eta)/(YBorn*eta - 100)) - charge*abs_protonBorn # Eq 55 in Shock and Helgeson 1990

    # find ion omega*10^-5, (J/mol) if charged
    HKFomega <- (10^-5)*(-1514.4*(Saq/4.184) + BZ)*4.184 # Eq 58 in Shock and Helgeson 1990

    ### METHOD FOR INORGANIC AQUEOUS ELECTROLYTES USING SHOCK AND HELGESON 1988:

        # find rej (angstroms), ions only
        #rej <- ((charge^2)*(eta*YBorn-100))/((Saq/4.184)-71.5*abs(charge)) # Eqs 46+56+57 in Shock and Helgeson 1988

        # find ion absolute omega*10^-5, (cal/mol)
        #HKFomega_abs_ion <- (eta*(charge^2))/rej # Eq 45 in Shock and Helgeson 1988

        # find ion omega*10^-5, (J/mol)
        #HKFomega2 <- (10^-5)*(HKFomega_abs_ion-(charge*abs_protonBorn))*4.184 # Eq 47 in Shock and Helgeson 1988

  } else {

    HKFomega <- NA

  }

  # find delta V solvation (cm3/mol)
  V_solv <- -(HKFomega/10^-5)*QBorn*10 # Eq 5 in Shock and Helgeson 1988, along with a conversion of 10 cm3 = 1 joule/bar

  # find delta V nonsolvation (cm3/mol)
  V_nonsolv <- Vh - V_solv # Eq 4 in Shock and Helgeson 1988

  # find sigma (cm3/mol)
  HKFsigma <- 1.11*V_nonsolv + 1.8 # Eq 87 in Shock and Helgeson

  # find delta cp solvation (J/mol*K)
  cp_solv <- ((HKFomega/10^-5)*298.15*XBorn) # Eq 35 in Shock and Helgeson 1988 dCpsolv = omega*T*X

  # find delta cp nonsolvation (J/mol*K)
  cp_nonsolv <- Cp - cp_solv # Eq 29 in Shock and Helgeson 1988



  if (is.na(Gh) != TRUE & charge == 0){ #
    # find a1*10 (j/mol*bar)
    HKFa1 <- (0.820-((1.858*10^-3)*(Gh)))*Vh # Eq 10 in Plyasunov and Shock 2001
    # why is this different than Eq 16 in Sverjensky et al 2014? Regardless, results seem to be very close using this eq vs. Eq 16.

    # find a2*10^-2 (j/mol)
    HKFa2 <- (0.648+((0.00481)*(Gh)))*Vh # Eq 11 in Plyasunov and Shock 2001

    # find a4*10^-4 (j*K/mol)
    HKFa4 <- 8.10-(0.746*HKFa2)+(0.219*Gh) # Eq 12 in Plyasunov and Shock 2001

  } else if (charge != 0){
    # find a1*10 (j/mol*bar)
    HKFa1 <- (0.1942*V_nonsolv + 1.52)*4.184 # Eq 16 in Sverjensky et al 2014, after Plyasunov and Shock 2001, converted to J/mol*bar. This equation is used in the DEW model since it works for charged and noncharged species up to 60kb

    # find a2*10^-2 (j/mol)
    HKFa2 <- (10^-2)*(((HKFsigma/41.84) - ((HKFa1/10)/4.184))/(1/(2601)))*4.184 # Eq 8 in Shock and Helgeson, rearranged to solve for a2*10^-2. Sigma is divided by 41.84 due to the conversion of 41.84 cm3 = cal/bar

    # find a4*10^-4 (j*K/mol)
    HKFa4 <- (10^-4)*(-4.134*(HKFa2/4.184)-27790)*4.184 # Eq 88 in Shock and Helgeson, solve for a4*10^-4

  } else {
    HKFa1 <- NA
    HKFa2 <- NA
    HKFa3 <- NA
  }

  # find c2*10^-4 (j*K/mol)
  if (is.na(Gh) != TRUE & charge == 0) {
    HKFc2 <- 21.4+(0.849*Gh) # Eq 14 in Plyasunov and Shock 2001
  } else if (is.na(Cp) != TRUE & charge != 0){
    HKFc2 <- (0.2037*(Cp/4.184) - 3.0346)*4.184 # Eq 89 in Shock and Helgeson 1988
  } else {
    HKFc2 <- NA
  }


  # find c1 (j/mol*K)
  HKFc1 <- cp_nonsolv-(((HKFc2)/10^-4)*(1/(298.15-228))^2) # Eq 31 in Shock and Helgeson 1988, rearranged to solve for c1

  # find a3 (j*K/mol*bar)
  HKFa3 <- (((Vh/10)-(HKFa1/10)-((HKFa2/10^-2)/2601)+((HKFomega/10^-5)*QBorn))/(1/(298.15-228)))-((HKFa4/10^-4)/2601) # Eq 11 in Shock and Helgeson 1988, rearranged to solve for a3. Vh is divided by 10 due to the conversion of 10 cm3 = J/bar

  # report results in calorie scale, ready to be pasted into OBIGT
  return(list(
  "G" = (Gf/4.184)*1000,
  "H" = (Hf/4.184)*1000,
  "S" = Saq/4.184,
  "Cp" = Cp/4.184,
  "V" = Vh,
  "a1" = HKFa1/4.184,
  "a2" = HKFa2/4.184,
  "a3" = HKFa3/4.184,
  "a4" = HKFa4/4.184,
  "c1" = HKFc1/4.184,
  "c2" = HKFc2/4.184,
  "omega" = HKFomega/4.184,
  "Z" = charge,
  "Vsolv" = V_solv,
  "Vnonsolv" = V_nonsolv,
  "sigma" = HKFsigma))
}

print("findHKF loaded...")

# a <- findHKF(Gh = -80.74, Vh = 68.16, Cp = 105, Gf = 5.795, Hf = -129.0, Saq = 76.6, charge = -1) # phenolate
# b <- findHKF(Gh = NA, Vh = 68.16, Cp = 105, Gf = 5.795, Hf = -129.0, Saq = 76.6, charge = -1) # phenolate 1988 method
# unlist(a)
# unlist(b)


# findHKF(Gh = NA, Vh = -25.4, Cp = -1.3*4.184, Gf = (-83500*4.184)/1000, Hf = (-91500*4.184)/1000, Saq = -55.7*4.184, charge = 2) # Be+2
# findHKF(Gh = NA, Vh = 18.13, Cp = 15.74*4.184, Gf = (-18990*4.184)/1000, Hf = (-31850*4.184)/1000, Saq = 26.57*4.184, charge = 1) # NH4+
# findHKF(Gh = NA, Vh = -0.87, Cp = 14.2*4.184, Gf = (-69933*4.184)/1000, Hf = (-66552*4.184)/1000, Saq = 2.70*4.184, charge = 1) # Li+

# Compare to table 4 of Plyasunov and Shock 2001
# (may be slightly different due to using Eq 16 in Sverjensky et al 2014 for calculating a1)
# findHKF(Gh = -0.51, Vh = 39.0, Cp = 146, Gf = NA, Hf = NA, Saq = NA, charge = 0) # SO2
# findHKF(Gh = -11.7, Vh = 77.1, Cp = 306, Gf = NA, Hf = NA, Saq = NA, charge = 0) # Pyridine
# findHKF(Gh = -37.7, Vh = 88.23, Cp = 347, Gf = NA, Hf = NA, Saq = NA, charge = 0) # 1,4-Butanediol
# findHKF(Gh = -74, Vh = 58.7, Cp = 76, Gf = NA, Hf = NA, Saq = NA, charge = 0) # beta-alanine
