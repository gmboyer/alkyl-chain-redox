library(CHNOSZ)

IPLthermo_directory <- "data//"
IPLworkbook_directory <- "data//"

thermo_method <- "mosaic() pe"
create_plots <- TRUE
res <- 800 

# Geochemical conditions of hot spring sites, styled as CHNOSZ input
my_basis <- c("H2O", "HCO3-", "NH3", "pH", "e-")

toplot <- c("Bison OF1", "Bison OF2", "Bison OF3", "Bison OF4", "Bison OF5", "Bison OF6",
            "Mound OF1", "Mound OF2", "Mound OF3", "Mound OF4", "Mound OF5",
            "Empress OF1", "Empress OF2", "Empress OF3", "Empress OF4", "Empress OF5",
            "Octopus OF1", "Octopus OF2")

redox_predicted_list <- c()
spring_list <- c()

for (spring in toplot){

  x_rng <- c(-10, -4) # (pe)
  by <- 0.2
  my_species <- c(9, 10, 11, 12, 13, 14, 15, 8, 16, 17, 18, 3, 4, 5, 6, 7, 2, 1) # all representative lipids
  if (spring == "Bison OF1"){
    my_activities <- c(0, -2.26, -5.41, 7.235, 0)
    my_T <- 89.0
  } else if (spring == "Bison OF2"){
    my_activities <- c(0, -2.27, -5.47, 7.338, 0)
    my_T <- 80.2
  } else if (spring == "Bison OF3"){
    my_activities <- c(0, -2.27, -5.65, 7.267, 0)
    my_T <- 73.7
  } else if (spring == "Bison OF4"){
    my_activities <- c(0, -2.27, -5.95, 8.089, 0)
    my_T <- 62.7
  } else if (spring == "Bison OF5"){
    my_activities <- c(0, -2.28, -6.26, 8.235, 0)
    my_T <- 37.2
  } else if (spring == "Bison OF6"){
    my_activities <- c(0, -2.29, -5.95, 9.007, 0)
    my_T <- 27.6
  } else if (spring == "Mound OF1"){
    my_activities <- c(0, -2.56, -5.41, 8.810, 0)
    my_T <- 91.0
  } else if (spring == "Mound OF2"){
    my_activities <- c(0, -2.55, -6.26, 8.648, 0)
    my_T <- 77.3
  } else if (spring == "Mound OF3"){
    my_activities <- c(0, -2.54, -5.77, 9.080, 0)
    my_T <- 64.8
  } else if (spring == "Mound OF4"){
    my_activities <- c(0, -2.55, -5.95, 9.223, 0)
    my_T <- 53.0
  } else if (spring == "Mound OF5"){
    my_basis <- c("H2O", "HCO3-", "NO3-", "pH", "e-") # NH4+ bdl, using NO3- instead
    my_activities <- c(0, -2.53, -6.01, 9.535, 0)
    my_T <- 35.1
  }else if (spring == "Empress OF1"){
    my_activities <- c(0, -2.48, -4.63, 5.785, 0)
    my_T <- 82.2
  } else if (spring == "Empress OF2"){
    next
  } else if (spring == "Empress OF3"){
    my_activities <- c(0, -2.65, -4.76, 7.630, 0)
    my_T <- 60.7
  } else if (spring == "Empress OF4"){
    my_activities <- c(0, -2.66, -4.67, 7.993, 0)
    my_T <- 51.6
  } else if (spring == "Empress OF5"){
    my_activities <- c(0, -2.71, -5.11, 8.425, 0)
    my_T <- 38.1
  } else if (spring == "Octopus OF1"){
    my_activities <- c(0, -2.27, -5.48, 7.289, 0)
    my_T <- 85.4
  } else if (spring == "Octopus OF2"){
    my_activities <- c(0, -2.28, -5.95, 8.266, 0)
    my_T <- 59.8
  }

  # create an OBIGT-style .csv that can be read by CHNOSZ and a thermo_results object file
  IPL_thermo(IPL_master, "mergedIPL 1", IPLthermo_directory,
    my_basis=my_basis, my_activities=my_activities, my_species=my_species,
    my_T=my_T, my_balance=1, res=res, x_rng=x_rng,
    save_object=TRUE, thermo_method=thermo_method,
    my_sample=spring, calc_Eh=TRUE, create_pub_table=TRUE)

  if(create_plots){
    print(paste("Creating line plot for", spring))
    redox_predicted <- create_lineplot_pe(spring, min = x_rng[1], max = x_rng[2], by = by)
    redox_predicted_list <- c(redox_predicted_list, redox_predicted)
    spring_list <- c(spring_list, spring)
  }

  # Create csv of alkyl chain predicted pe
  if(length(toplot) == 18 & spring == toplot[length(toplot)]){
      write.table(data.frame(sample = spring_list, Lip_pe = redox_predicted_list), paste(table_name,  "pe.csv"), sep = ",", row.names = FALSE)
  }

} # end for(spring in toplot)
